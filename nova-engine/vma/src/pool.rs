/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::flags::PoolCreateFlag;

use vma_sys::raw;

use std::marker::PhantomData;

use ash::vk;
//==================================================================================================
///
/// Builder for PoolCreateInfo
///
pub struct PoolCreateInfoBuilder {
    create_info: PoolCreateInfo,
}

//==================================================================================================
impl PoolCreateInfoBuilder {
    ///
    ///
    ///
    pub fn new() -> Self {
        PoolCreateInfoBuilder {
            create_info: PoolCreateInfo {
                memory_type_index: 0,
                flags: PoolCreateFlag::from(0u32),
                block_size: 0,
                min_block_count: 0,
                max_block_count: 0,
                frame_in_use_count: 0,
            },
        }
    }

    ///
    ///
    ///
    pub fn memory_type_index(mut self, index: u32) -> Self {
        self.create_info.memory_type_index = index;
        self
    }

    ///
    ///
    ///
    pub fn flags(mut self, flags: PoolCreateFlag) -> Self {
        self.create_info.flags = flags;
        self
    }

    ///
    ///
    ///
    pub fn block_size(mut self, size: vk::DeviceSize) -> Self {
        self.create_info.block_size = size;
        self
    }

    ///
    ///
    ///
    pub fn min_block_count(mut self, count: usize) -> Self {
        self.create_info.min_block_count = count;
        self
    }

    ///
    ///
    ///
    pub fn max_block_count(mut self, count: usize) -> Self {
        self.create_info.max_block_count = count;
        self
    }

    ///
    ///
    ///
    pub fn frame_in_use_count(mut self, count: u32) -> Self {
        self.create_info.frame_in_use_count = count;
        self
    }

    ///
    ///
    ///
    pub fn build(self) -> PoolCreateInfo {
        self.create_info
    }
}

//==================================================================================================
impl Default for PoolCreateInfoBuilder {
    fn default() -> Self {
        Self::new()
    }
}

//==================================================================================================
///
/// VmaPoolCreateInfo
///
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct PoolCreateInfo {
    memory_type_index: u32,
    flags: PoolCreateFlag,
    block_size: vk::DeviceSize,
    min_block_count: usize,
    max_block_count: usize,
    frame_in_use_count: u32,
}

//==================================================================================================
impl PoolCreateInfo {
    ///
    ///
    ///
    pub fn builder() -> PoolCreateInfoBuilder {
        PoolCreateInfoBuilder::new()
    }
}

//==================================================================================================
///
/// VmaPool
///
#[derive(Debug, Clone)]
pub struct Pool<'alloc> {
    pool: raw::VmaPool,
    allocator: PhantomData<&'alloc ()>,
}

//==================================================================================================
impl<'alloc> Pool<'alloc> {
    ///
    /// Create from a raw handle.
    ///
    /// Must have a lifetime shorter than it's parent allocator
    ///
    pub fn from_raw(pool: raw::VmaPool) -> Self {
        Pool::<'alloc> {
            pool,
            allocator: PhantomData,
        }
    }

    ///
    /// Returns the underlying raw::VmaPool for use with raw function calls
    ///
    /// WARNING: Will discard lifetime information
    ///
    pub fn into_raw(self) -> raw::VmaPool {
        self.pool
    }
}
