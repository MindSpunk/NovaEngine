/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use std::os::raw::c_uint;

///
/// FMOD_STUDIO_PARAMETER_ID
///
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct ParameterID {
    pub data_1: c_uint,
    pub data_2: c_uint,
}

impl ParameterID {
    pub fn from_ffi(ffi: raw::FMOD_STUDIO_PARAMETER_ID) -> Self {
        Self {
            data_1: ffi.data1,
            data_2: ffi.data2,
        }
    }

    pub fn into_ffi(self) -> raw::FMOD_STUDIO_PARAMETER_ID {
        raw::FMOD_STUDIO_PARAMETER_ID {
            data1: self.data_1,
            data2: self.data_2,
        }
    }
}
