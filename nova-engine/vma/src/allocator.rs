/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::allocation::Allocation;
use crate::allocation::AllocationCreateInfo;
use crate::allocation::AllocationInfo;
use crate::enums::AllocatorBuilderError;
use crate::flags::AllocatorCreateFlag;
use crate::pool::Pool;
use crate::pool::PoolCreateInfo;
use crate::stats::Stats;
use crate::utils;
use crate::vulkan_functions::VulkanFunctionsBuilder;

use ash::version::DeviceV1_0;
use ash::version::InstanceV1_0;
use ash::vk;

use vma_sys::raw;

use core::mem;
use core::ptr;

use std::ffi::c_void;
use std::ffi::CStr;
use std::os::raw::c_char;

//==================================================================================================
///
/// Builder utility for simple, safe construction of a vma Allocator
///
pub struct AllocatorBuilder<'inst, 'dev> {
    create_info: raw::VmaAllocatorCreateInfo,
    dev: Option<&'dev vk::DeviceFnV1_0>,
    inst: Option<&'inst vk::InstanceFnV1_0>,
}

//==================================================================================================
impl<'inst, 'dev> AllocatorBuilder<'inst, 'dev> {
    ///
    /// New instance of the builder
    ///
    /// User must give builder an instance, device and physical device otherwise it will fail to
    /// build
    ///
    pub fn new() -> Self {
        let create_info = raw::VmaAllocatorCreateInfo {
            flags: 0,
            physicalDevice: ptr::null_mut(),
            device: ptr::null_mut(),
            preferredLargeHeapBlockSize: 0,
            pAllocationCallbacks: ptr::null_mut(),
            pDeviceMemoryCallbacks: ptr::null_mut(),
            frameInUseCount: 0,
            pHeapSizeLimit: ptr::null_mut(),
            pVulkanFunctions: ptr::null_mut(),
            pRecordSettings: ptr::null_mut(),
        };

        AllocatorBuilder {
            create_info,
            dev: None,
            inst: None,
        }
    }

    ///
    ///
    ///
    pub fn instance(mut self, inst: &'inst ash::Instance) -> Self {
        self.inst = Some(inst.fp_v1_0());
        self
    }

    ///
    ///
    ///
    pub fn device(mut self, dev: &'dev ash::Device) -> Self {
        self.create_info.device = unsafe { mem::transmute(dev.handle()) };
        self.dev = Some(dev.fp_v1_0());
        self
    }

    ///
    ///
    ///
    pub fn physical_device(mut self, physical_device: vk::PhysicalDevice) -> Self {
        self.create_info.physicalDevice = unsafe { mem::transmute(physical_device) };
        self
    }

    ///
    /// Build the VmaAllocator
    ///
    /// ## Errors
    ///
    /// - If there is no valid physical device handle
    /// - If there is no valid device handle
    /// - If not all required function pointers are provided
    /// - If KHR_DEDICATED_ALLOCATION_BIT is set and the required function pointers are not given
    ///
    pub unsafe fn build(mut self) -> Result<Allocator, AllocatorBuilderError> {
        if self.create_info.physicalDevice.is_null() {
            return Err(AllocatorBuilderError::InvalidPhysicalDevice);
        }

        if self.create_info.device.is_null() || self.dev.is_none() {
            return Err(AllocatorBuilderError::InvalidDevice);
        }

        if self.inst.is_none() {
            return Err(AllocatorBuilderError::InvalidInstance);
        }

        let functions = VulkanFunctionsBuilder::new()
            .ash_tables(self.inst.unwrap(), self.dev.unwrap())
            .build();

        if !utils::allocator_functions_valid(&functions) {
            return Err(AllocatorBuilderError::InvalidFunctionPointers);
        }

        self.create_info.pVulkanFunctions = &functions;

        let a_val: u32 = AllocatorCreateFlag::KHR_DEDICATED_ALLOCATION_BIT.into();
        if self.create_info.flags & a_val != 0
            && (functions.vkGetBufferMemoryRequirements2KHR == None
                || functions.vkGetImageMemoryRequirements2KHR == None)
        {
            return Err(AllocatorBuilderError::InvalidExtensionFunctionPointers);
        }

        let raw_alloc: raw::VmaAllocator = ptr::null_mut();

        let mut alloc = Allocator {
            allocator: raw_alloc,
        };

        let raw_alloc_ptr = &mut alloc.allocator;
        let raw_alloc_ptr = raw_alloc_ptr as *mut raw::VmaAllocator;
        raw::vmaCreateAllocator(&self.create_info, raw_alloc_ptr);

        Ok(alloc)
    }
}

//==================================================================================================
impl<'inst, 'dev> Default for AllocatorBuilder<'inst, 'dev> {
    fn default() -> Self {
        Self::new()
    }
}

//==================================================================================================
///
/// Holds the internal reference to the vma allocator as well as managing correct lifetime
///
#[repr(transparent)]
pub struct Allocator {
    allocator: raw::VmaAllocator,
}

//==================================================================================================
impl Allocator {
    ///
    /// Return a builder for the vma allocator
    ///
    pub fn builder<'inst, 'dev>() -> AllocatorBuilder<'inst, 'dev> {
        AllocatorBuilder::new()
    }

    ///
    /// Cleanup the internal handles and close the allocator
    ///
    pub unsafe fn destroy(&mut self) {
        raw::vmaDestroyAllocator(self.allocator);
        self.allocator = ptr::null_mut();
    }

    ///
    /// vmaGetPhysicalDeviceProperties
    ///
    pub unsafe fn get_physical_device_properties<'alloc>(
        &'alloc self,
    ) -> &'alloc vk::PhysicalDeviceProperties {
        let mut reference: *const vk::PhysicalDeviceProperties = ptr::null_mut();
        let reference_ptr = &mut reference as *mut *const vk::PhysicalDeviceProperties;
        let reference_ptr = reference_ptr as *mut *const raw::VkPhysicalDeviceProperties;

        raw::vmaGetPhysicalDeviceProperties(self.allocator, reference_ptr);

        reference
            .as_ref::<'alloc>()
            .expect("Given nullptr by vmaGetPhysicalDeviceProperties")
    }

    ///
    /// vmaGetPhysicalDeviceProperties
    ///
    pub unsafe fn get_memory_properties<'alloc>(
        &'alloc self,
    ) -> &'alloc vk::PhysicalDeviceMemoryProperties {
        let mut reference: *const vk::PhysicalDeviceMemoryProperties = ptr::null_mut();
        let reference_ptr = &mut reference as *mut *const vk::PhysicalDeviceMemoryProperties;
        let reference_ptr = reference_ptr as *mut *const raw::VkPhysicalDeviceMemoryProperties;

        raw::vmaGetMemoryProperties(self.allocator, reference_ptr);

        reference
            .as_ref::<'alloc>()
            .expect("Given nullptr by vmaGetMemoryProperties")
    }

    ///
    /// vmaCalculateStats
    ///
    pub unsafe fn calculate_stats(&self) -> Stats {
        let mut stats = Stats::default();
        let stats_ptr = &mut stats as *mut Stats;
        let stats_ptr = stats_ptr as *mut raw::VmaStats;

        raw::vmaCalculateStats(self.allocator, stats_ptr);

        stats
    }

    ///
    /// Returns a rusty String object that is much easier to pass around than a CStr reference. It
    /// will also automatically de-allocate making it much safer, but will incur an extra allocation
    /// and utf-8 conversion so be aware of the overhead.
    ///
    pub unsafe fn get_stats_string(&self, detailed_map: bool) -> String {
        let c_str = self.build_stats_string(detailed_map);

        let string = c_str.to_str().expect("Invalid utf-8 chars in stats string");
        let string = string.to_string();

        self.free_stats_string(c_str);

        string
    }

    ///
    /// vmaBuildStatsString
    ///
    pub unsafe fn build_stats_string(&self, detailed_map: bool) -> &CStr {
        let mut c_str_ptr: *mut c_char = ptr::null_mut();

        raw::vmaBuildStatsString(
            self.allocator,
            &mut c_str_ptr as *mut *mut c_char,
            if detailed_map { vk::TRUE } else { vk::FALSE },
        );

        CStr::from_ptr(c_str_ptr)
    }

    ///
    /// vmaFreeStatsString
    ///
    pub unsafe fn free_stats_string(&self, str: &CStr) {
        let c_str_ptr = str.as_ptr();
        raw::vmaFreeStatsString(self.allocator, c_str_ptr as *mut c_char);
    }

    ///
    /// vmaFindMemoryTypeIndex
    ///
    pub unsafe fn find_memory_type_index(
        &self,
        memory_type_bits: u32,
        allocation_create_info: &AllocationCreateInfo,
    ) -> Result<u32, vk::Result> {
        let mut idx = 0u32;

        let alloc_ptr = allocation_create_info as *const AllocationCreateInfo;
        let alloc_ptr = alloc_ptr as *const raw::VmaAllocationCreateInfo;

        let result = raw::vmaFindMemoryTypeIndex(
            self.allocator,
            memory_type_bits,
            alloc_ptr,
            &mut idx as *mut u32,
        );

        if result as i32 == vk::Result::ERROR_FEATURE_NOT_PRESENT.as_raw() {
            return Err(vk::Result::from_raw(result as i32));
        }

        Ok(idx)
    }

    ///
    /// vmaFindMemoryTypeIndexForBufferInfo
    ///
    pub unsafe fn find_memory_type_index_for_buffer_info(
        &self,
        buffer_create_info: &vk::BufferCreateInfo,
        allocation_create_info: &AllocationCreateInfo,
    ) -> Result<u32, vk::Result> {
        let mut idx = 0u32;

        let alloc_ptr = allocation_create_info as *const AllocationCreateInfo;
        let alloc_ptr = alloc_ptr as *const raw::VmaAllocationCreateInfo;

        let buffer_ptr = buffer_create_info as *const vk::BufferCreateInfo;
        let buffer_ptr = buffer_ptr as *const raw::VkBufferCreateInfo;

        let result = raw::vmaFindMemoryTypeIndexForBufferInfo(
            self.allocator,
            buffer_ptr,
            alloc_ptr,
            &mut idx as *mut u32,
        );

        if result as i32 == vk::Result::ERROR_FEATURE_NOT_PRESENT.as_raw() {
            return Err(vk::Result::from_raw(result as i32));
        }

        Ok(idx)
    }

    ///
    /// vmaFindMemoryTypeIndexForImageInfo
    ///
    pub unsafe fn find_memory_type_index_for_image_info(
        &self,
        image_create_info: &vk::ImageCreateInfo,
        allocation_create_info: &AllocationCreateInfo,
    ) -> Result<u32, vk::Result> {
        let mut idx = 0u32;

        let alloc_ptr = allocation_create_info as *const AllocationCreateInfo;
        let alloc_ptr = alloc_ptr as *const raw::VmaAllocationCreateInfo;

        let image_ptr = image_create_info as *const vk::ImageCreateInfo;
        let image_ptr = image_ptr as *const raw::VkImageCreateInfo;

        let result = raw::vmaFindMemoryTypeIndexForImageInfo(
            self.allocator,
            image_ptr,
            alloc_ptr,
            &mut idx as *mut u32,
        );

        if result as i32 == vk::Result::ERROR_FEATURE_NOT_PRESENT.as_raw() {
            return Err(vk::Result::from_raw(result as i32));
        }

        Ok(idx)
    }

    ///
    /// vmaAllocateMemory
    ///
    pub unsafe fn allocate_memory(
        &self,
        memory_requirements: &vk::MemoryRequirements,
        create_info: &AllocationCreateInfo,
    ) -> Result<Allocation, vk::Result> {
        let mut allocation: raw::VmaAllocation = ptr::null_mut();

        let create_info_ptr = create_info as *const AllocationCreateInfo;
        let create_info_ptr = create_info_ptr as *const raw::VmaAllocationCreateInfo;

        let requirements = memory_requirements as *const vk::MemoryRequirements;
        let requirements = requirements as *const raw::VkMemoryRequirements;

        let result = raw::vmaAllocateMemory(
            self.allocator,
            requirements,
            create_info_ptr,
            &mut allocation,
            ptr::null_mut(),
        );

        if result as i32 != vk::Result::SUCCESS.as_raw() {
            return Err(vk::Result::from_raw(result as i32));
        }

        let ret = Allocation::from_raw(allocation);
        Ok(ret)
    }

    ///
    /// vmaAllocateMemoryPages
    ///
    pub unsafe fn allocate_memory_pages(
        &self,
        memory_requirements: &vk::MemoryRequirements,
        create_info: &AllocationCreateInfo,
        allocation_count: usize,
    ) -> Result<Vec<Allocation>, vk::Result> {
        let mut ret: Vec<Allocation> = Vec::with_capacity(allocation_count);
        ret.resize(allocation_count, Allocation::from_raw(ptr::null_mut()));

        let create_info_ptr = create_info as *const AllocationCreateInfo;
        let create_info_ptr = create_info_ptr as *const raw::VmaAllocationCreateInfo;

        let requirements = memory_requirements as *const vk::MemoryRequirements;
        let requirements = requirements as *const raw::VkMemoryRequirements;

        let result = raw::vmaAllocateMemoryPages(
            self.allocator,
            requirements,
            create_info_ptr,
            allocation_count,
            ret.as_mut_ptr() as *mut raw::VmaAllocation,
            ptr::null_mut(),
        );

        if result as i32 != vk::Result::SUCCESS.as_raw() {
            return Err(vk::Result::from_raw(result as i32));
        }

        Ok(ret)
    }

    ///
    /// vmaAllocateMemoryForBuffer
    ///
    pub unsafe fn allocate_memory_for_buffer(
        &self,
        buffer: vk::Buffer,
        create_info: &AllocationCreateInfo,
    ) -> Result<Allocation, vk::Result> {
        let mut allocation: raw::VmaAllocation = ptr::null_mut();

        let create_info_ptr = create_info as *const AllocationCreateInfo;
        let create_info_ptr = create_info_ptr as *const raw::VmaAllocationCreateInfo;

        let result = raw::vmaAllocateMemoryForBuffer(
            self.allocator,
            mem::transmute(buffer),
            create_info_ptr,
            &mut allocation,
            ptr::null_mut(),
        );

        if result as i32 != vk::Result::SUCCESS.as_raw() {
            return Err(vk::Result::from_raw(result as i32));
        }

        let ret = Allocation::from_raw(allocation);
        Ok(ret)
    }

    ///
    /// vmaAllocateMemoryforImage
    ///
    pub unsafe fn allocate_memory_for_image(
        &self,
        image: vk::Image,
        create_info: &AllocationCreateInfo,
    ) -> Result<Allocation, vk::Result> {
        let mut allocation: raw::VmaAllocation = ptr::null_mut();

        let create_info_ptr = create_info as *const AllocationCreateInfo;
        let create_info_ptr = create_info_ptr as *const raw::VmaAllocationCreateInfo;

        let result = raw::vmaAllocateMemoryForImage(
            self.allocator,
            mem::transmute(image),
            create_info_ptr,
            &mut allocation,
            ptr::null_mut(),
        );

        if result as i32 != vk::Result::SUCCESS.as_raw() {
            return Err(vk::Result::from_raw(result as i32));
        }

        let ret = Allocation::from_raw(allocation);
        Ok(ret)
    }

    ///
    /// vmaFreeMemory
    ///
    pub unsafe fn free_memory(&self, allocation: Allocation) {
        raw::vmaFreeMemory(self.allocator, allocation.into_raw());
    }

    ///
    /// vmaFreeMemoryPages
    ///
    pub unsafe fn free_memory_pages(&self, allocations: &[Allocation]) {
        let pointer = allocations.as_ptr();
        let pointer = pointer as *mut Allocation;
        let pointer = pointer as *mut raw::VmaAllocation;

        raw::vmaFreeMemoryPages(self.allocator, allocations.len(), pointer)
    }

    ///
    /// vmaResizeAllocation
    ///
    pub unsafe fn resize_allocation(
        &self,
        allocation: &Allocation,
        new_size: vk::DeviceSize,
    ) -> Result<(), vk::Result> {
        let result =
            raw::vmaResizeAllocation(self.allocator, allocation.clone().into_raw(), new_size);

        if result as i32 != vk::Result::SUCCESS.as_raw() {
            return Err(vk::Result::from_raw(result as i32));
        }

        Ok(())
    }

    ///
    /// vmaGetAllocationInfo
    ///
    pub unsafe fn get_allocation_info(&self, allocation: &Allocation) -> AllocationInfo {
        let mut info = AllocationInfo {
            memory_type: 0,
            device_memory: vk::DeviceMemory::default(),
            offset: 0,
            size: 0,
            p_mapped_data: ptr::null_mut(),
            p_user_data: ptr::null_mut(),
        };
        let info_ptr = &mut info as *mut AllocationInfo;
        let info_ptr = info_ptr as *mut raw::VmaAllocationInfo;

        raw::vmaGetAllocationInfo(self.allocator, allocation.clone().into_raw(), info_ptr);

        info
    }

    ///
    /// vmaTouchAllocation
    ///
    pub unsafe fn touch_allocation(&self, allocation: &Allocation) -> bool {
        let result = raw::vmaTouchAllocation(self.allocator, allocation.clone().into_raw());

        result == vk::TRUE
    }

    // TODO : vmaSetAllocationUserData (PROBABLY WONT EXPOSE THIS, NOT VERY RUSTY)

    ///
    /// vmaCreateLostAllocation
    ///
    pub unsafe fn create_lost_allocation(&self) -> Allocation {
        let mut allocation: raw::VmaAllocation = ptr::null_mut();
        raw::vmaCreateLostAllocation(self.allocator, &mut allocation as *mut raw::VmaAllocation);

        let allocation: Allocation = mem::transmute(allocation);
        allocation
    }

    ///
    /// vmaMapMemory
    ///
    pub unsafe fn map_memory(&self, allocation: &Allocation) -> Result<*mut u8, vk::Result> {
        let mut pointer: *mut u8 = ptr::null_mut();
        let pointer_pointer = &mut pointer as *mut *mut u8;
        let pointer_pointer = pointer_pointer as *mut *mut c_void;

        let result = raw::vmaMapMemory(
            self.allocator,
            allocation.clone().into_raw(),
            pointer_pointer,
        );

        if result as i32 != vk::Result::SUCCESS.as_raw() {
            return Err(vk::Result::from_raw(result as i32));
        }

        Ok(pointer)
    }

    ///
    /// vmaUnmapMemory
    ///
    pub unsafe fn unmap_memory(&self, allocation: &Allocation) {
        raw::vmaUnmapMemory(self.allocator, allocation.clone().into_raw());
    }

    ///
    /// vmaFlushAllocation
    ///
    pub unsafe fn flush_allocation(
        &self,
        allocation: &Allocation,
        offset: vk::DeviceSize,
        size: vk::DeviceSize,
    ) {
        raw::vmaFlushAllocation(self.allocator, allocation.clone().into_raw(), offset, size);
    }
    ///
    /// vmaInvalidateAllocation
    ///
    pub unsafe fn invalidate_allocation(
        &self,
        allocation: &Allocation,
        offset: vk::DeviceSize,
        size: vk::DeviceSize,
    ) {
        raw::vmaInvalidateAllocation(self.allocator, allocation.clone().into_raw(), offset, size);
    }

    ///
    /// vmaCheckCorruption
    ///
    pub unsafe fn check_corruption(&self, memory_type_bits: u32) -> Result<(), vk::Result> {
        let result = raw::vmaCheckCorruption(self.allocator, memory_type_bits);

        if result as i32 != vk::Result::SUCCESS.as_raw() {
            return Err(vk::Result::from_raw(result as i32));
        }

        Ok(())
    }

    // TODO : vmaDefragmentationBegin
    // TODO : vmaDefragmentationEnd
    // TODO : vmaDefragment (Deprecated)

    ///
    /// vmaBindBufferMemory
    ///
    pub unsafe fn bind_buffer_memory(
        &self,
        allocation: &Allocation,
        buffer: vk::Buffer,
    ) -> Result<(), vk::Result> {
        let result = raw::vmaBindBufferMemory(
            self.allocator,
            allocation.clone().into_raw(),
            mem::transmute(buffer),
        );

        if result as i32 != vk::Result::SUCCESS.as_raw() {
            return Err(vk::Result::from_raw(result as i32));
        }

        Ok(())
    }

    ///
    /// vmaBindImageMemory
    ///
    pub unsafe fn bind_image_memory(
        &self,
        allocation: &Allocation,
        image: vk::Image,
    ) -> Result<(), vk::Result> {
        let result = raw::vmaBindImageMemory(
            self.allocator,
            allocation.clone().into_raw(),
            mem::transmute(image),
        );

        if result as i32 != vk::Result::SUCCESS.as_raw() {
            return Err(vk::Result::from_raw(result as i32));
        }

        Ok(())
    }

    ///
    /// vmaCreateBuffer
    ///
    pub unsafe fn create_buffer(
        &self,
        buffer_create_info: &vk::BufferCreateInfo,
        alloc_create_info: &AllocationCreateInfo,
    ) -> Result<(vk::Buffer, Allocation), vk::Result> {
        let b_create_info_ptr = buffer_create_info as *const vk::BufferCreateInfo;
        let b_create_info_ptr = b_create_info_ptr as *const raw::VkBufferCreateInfo;

        let a_create_info_ptr = alloc_create_info as *const AllocationCreateInfo;
        let a_create_info_ptr = a_create_info_ptr as *const raw::VmaAllocationCreateInfo;

        let mut buffer: vk::Buffer = vk::Buffer::null();
        let buffer_ptr = &mut buffer as *mut vk::Buffer;
        let buffer_ptr = buffer_ptr as *mut raw::VkBuffer;

        let mut allocation: raw::VmaAllocation = ptr::null_mut();
        let allocation_ptr = &mut allocation as *mut raw::VmaAllocation;

        let result = raw::vmaCreateBuffer(
            self.allocator,
            b_create_info_ptr,
            a_create_info_ptr,
            buffer_ptr,
            allocation_ptr,
            ptr::null_mut(),
        );

        if result as i32 != vk::Result::SUCCESS.as_raw() {
            return Err(vk::Result::from_raw(result as i32));
        }

        Ok((buffer, Allocation::from_raw(allocation)))
    }

    ///
    /// vmaDestroyBuffer
    ///
    pub unsafe fn destroy_buffer(&self, buffer: vk::Buffer, alloc: Allocation) {
        raw::vmaDestroyBuffer(
            self.allocator,
            mem::transmute(buffer),
            alloc.clone().into_raw(),
        );
        drop(alloc);
    }

    ///
    /// vmaCreateImage
    ///
    pub unsafe fn create_image(
        &self,
        image_create_info: &vk::ImageCreateInfo,
        alloc_create_info: &AllocationCreateInfo,
    ) -> Result<(vk::Image, Allocation), vk::Result> {
        let i_create_info_ptr = image_create_info as *const vk::ImageCreateInfo;
        let i_create_info_ptr = i_create_info_ptr as *const raw::VkImageCreateInfo;

        let a_create_info_ptr = alloc_create_info as *const AllocationCreateInfo;
        let a_create_info_ptr = a_create_info_ptr as *const raw::VmaAllocationCreateInfo;

        let mut image: vk::Image = vk::Image::null();
        let image_ptr = &mut image as *mut vk::Image;
        let image_ptr = image_ptr as *mut raw::VkImage;

        let mut allocation: raw::VmaAllocation = ptr::null_mut();
        let allocation_ptr = &mut allocation as *mut raw::VmaAllocation;

        let result = raw::vmaCreateImage(
            self.allocator,
            i_create_info_ptr,
            a_create_info_ptr,
            image_ptr,
            allocation_ptr,
            ptr::null_mut(),
        );

        if result as i32 != vk::Result::SUCCESS.as_raw() {
            return Err(vk::Result::from_raw(result as i32));
        }

        Ok((image, Allocation::from_raw(allocation)))
    }

    ///
    /// vmaDestroyImage
    ///
    pub unsafe fn destroy_image(&self, buffer: vk::Image, alloc: Allocation) {
        raw::vmaDestroyImage(
            self.allocator,
            mem::transmute(buffer),
            alloc.clone().into_raw(),
        );
        drop(alloc);
    }

    ///
    /// vmaSetCurrentFrameIndex
    ///
    pub unsafe fn set_current_frame_index(self, index: u32) {
        raw::vmaSetCurrentFrameIndex(self.allocator, index);
    }

    ///
    /// vmaCreatePool
    ///
    pub unsafe fn create_pool<'alloc>(
        &'alloc self,
        pool_create_info: &PoolCreateInfo,
    ) -> Result<Pool<'alloc>, vk::Result> {
        let mut pool: raw::VmaPool = ptr::null_mut();

        let create_ptr = pool_create_info as *const PoolCreateInfo;
        let create_ptr = create_ptr as *const raw::VmaPoolCreateInfo;

        let result = raw::vmaCreatePool(self.allocator, create_ptr, &mut pool as *mut raw::VmaPool);

        if result as i32 != vk::Result::SUCCESS.as_raw() {
            return Err(vk::Result::from_raw(result as i32));
        }

        Ok(Pool::<'alloc>::from_raw(pool))
    }

    ///
    /// vmaDestroyPool
    ///
    pub unsafe fn destroy_pool(&self, pool: Pool) {
        raw::vmaDestroyPool(self.allocator, mem::transmute(pool));
    }

    ///
    /// vmaGetPoolStats
    ///
    pub unsafe fn get_pool_stats(&self, pool: &Pool) -> raw::VmaPoolStats {
        let mut stats = raw::VmaPoolStats {
            size: 0,
            unusedSize: 0,
            allocationCount: 0,
            unusedRangeCount: 0,
            unusedRangeSizeMax: 0,
            blockCount: 0,
        };

        let raw_pool = pool.clone().into_raw();

        raw::vmaGetPoolStats(
            self.allocator,
            raw_pool,
            &mut stats as *mut raw::VmaPoolStats,
        );

        stats
    }

    ///
    /// vmaMakePoolAllocationsLost
    ///
    pub unsafe fn make_pool_allocations_lost(&self, pool: &Pool) -> usize {
        let mut out = 0usize;
        let raw_pool = pool.clone().into_raw();

        raw::vmaMakePoolAllocationsLost(self.allocator, raw_pool, &mut out as *mut usize);

        out
    }

    pub unsafe fn check_pool_corruption(&self, pool: &Pool) -> Result<(), vk::Result> {
        let result = raw::vmaCheckPoolCorruption(self.allocator, pool.clone().into_raw());

        if result as i32 != vk::Result::SUCCESS.as_raw() {
            return Err(vk::Result::from_raw(result as i32));
        }

        Ok(())
    }
}

//
// Implementing these is safe because vma internally synchronizes access
//
unsafe impl Send for Allocator {}
unsafe impl Sync for Allocator {}

impl Drop for Allocator {
    fn drop(&mut self) {
        if !self.allocator.is_null() {
            panic!("vma::Allocator dropped without being destroyed");
        }
    }
}
