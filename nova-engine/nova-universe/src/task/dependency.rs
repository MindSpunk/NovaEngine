/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::resource::ResourceID;
use crate::{Node, Resource};
use std::any::TypeId;

// =================================================================================================
///
/// An enum used to uniquely identify items accessible to tasks to be used to build the task graph
///
#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub struct Dependency {
    pub(crate) inner: DependencyInner,
}

// =================================================================================================
///
/// An enum used to uniquely identify items accessible to tasks to be used to build the task graph
///
#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub enum DependencyInner {
    NodeType(TypeId),
    Transforms,
    Matrices,
    Resource(ResourceID),
    NodeHierarchy,
    Task(usize),
}

impl Dependency {
    ///
    /// Creates a new dependable for a given node type
    ///
    #[inline]
    pub fn node<T: Node>() -> Self {
        Self {
            inner: DependencyInner::NodeType(TypeId::of::<T>()),
        }
    }

    ///
    /// Creates a new dependable for a given node type's transforms
    ///
    #[inline]
    pub fn transforms() -> Self {
        Self {
            inner: DependencyInner::Transforms,
        }
    }

    ///
    /// Creates a new dependable for a given node type's matrices
    ///
    #[inline]
    pub fn matrices() -> Self {
        Self {
            inner: DependencyInner::Matrices,
        }
    }

    ///
    /// Creates a new dependable for a given resource
    ///
    #[inline]
    pub fn resource<T: Resource>() -> Self {
        Self {
            inner: DependencyInner::Resource(ResourceID::of::<T>()),
        }
    }

    ///
    /// Creates a new dependable for a given resource
    ///
    #[inline]
    pub fn node_hierarchy() -> Self {
        Self {
            inner: DependencyInner::NodeHierarchy,
        }
    }

    ///
    /// Creates a new dependable for a given task
    ///
    #[inline]
    pub(crate) unsafe fn new_task(task_id: usize) -> Self {
        Self {
            inner: DependencyInner::Task(task_id),
        }
    }
}

// =================================================================================================
///
///
///
pub struct Dependencies {
    pub(crate) reads: Vec<Dependency>,
    pub(crate) writes: Vec<Dependency>,
}

impl Dependencies {
    ///
    ///
    ///
    pub fn read(&mut self, read: Dependency) {
        self.reads.push(read);
    }

    ///
    ///
    ///
    pub fn write(&mut self, write: Dependency) {
        self.writes.push(write);
    }
}
