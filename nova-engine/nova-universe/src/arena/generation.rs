/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use std::hash::Hash;

// =================================================================================================
///
/// A wrapper for a generation used in generational indexing. Used to ensure that indexes point to
/// the object they were allocated from.
///
/// This guards from a situation where an index points into a space where the original object had
/// been freed and replaced with a new object in the same place.
///
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub struct Generation(i32);

impl Generation {
    pub fn new() -> Self {
        Self(0)
    }

    #[inline]
    pub fn is_alive(&self) -> bool {
        self.0 > 0
    }

    #[inline]
    pub fn is_dead(&self) -> bool {
        !self.is_alive()
    }

    #[inline]
    pub fn resurrect(&mut self) {
        debug_assert!(self.is_dead());
        self.0 = 1 - self.0;
    }

    #[inline]
    pub fn kill(&mut self) {
        debug_assert!(self.is_alive());
        self.0 = -self.0;
    }
}

impl Default for Generation {
    fn default() -> Self {
        Generation::new()
    }
}
