/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use raw::FMOD_STUDIO_LOADING_STATE;

///
/// FMOD_STUDIO_LOADING_STATE
///
#[repr(u32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum LoadingState {
    Unloading = 0,
    Unloaded = 1,
    Loading = 2,
    Loaded = 3,
    Error = 4,
}

impl LoadingState {
    pub fn from_ffi(ffi: FMOD_STUDIO_LOADING_STATE) -> Self {
        match ffi {
            FMOD_STUDIO_LOADING_STATE::FMOD_STUDIO_LOADING_STATE_UNLOADING => {
                LoadingState::Unloading
            }
            FMOD_STUDIO_LOADING_STATE::FMOD_STUDIO_LOADING_STATE_UNLOADED => LoadingState::Unloaded,
            FMOD_STUDIO_LOADING_STATE::FMOD_STUDIO_LOADING_STATE_LOADING => LoadingState::Loading,
            FMOD_STUDIO_LOADING_STATE::FMOD_STUDIO_LOADING_STATE_LOADED => LoadingState::Loaded,
            FMOD_STUDIO_LOADING_STATE::FMOD_STUDIO_LOADING_STATE_ERROR => LoadingState::Error,
            FMOD_STUDIO_LOADING_STATE::FMOD_STUDIO_LOADING_STATE_FORCEINT => unreachable!(),
        }
    }

    pub fn into_ffi(self) -> FMOD_STUDIO_LOADING_STATE {
        match self {
            LoadingState::Unloading => {
                FMOD_STUDIO_LOADING_STATE::FMOD_STUDIO_LOADING_STATE_UNLOADING
            }
            LoadingState::Unloaded => FMOD_STUDIO_LOADING_STATE::FMOD_STUDIO_LOADING_STATE_UNLOADED,
            LoadingState::Loading => FMOD_STUDIO_LOADING_STATE::FMOD_STUDIO_LOADING_STATE_LOADING,
            LoadingState::Loaded => FMOD_STUDIO_LOADING_STATE::FMOD_STUDIO_LOADING_STATE_LOADED,
            LoadingState::Error => FMOD_STUDIO_LOADING_STATE::FMOD_STUDIO_LOADING_STATE_ERROR,
        }
    }
}
