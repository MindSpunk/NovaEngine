/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

mod access;
mod dependency;
mod graph;

pub use self::access::DataAccess;
pub use self::access::DataAccessState;
pub use self::access::Matrices;
pub use self::access::MatricesMut;
pub use self::access::MatrixGroup;
pub use self::access::MatrixGroupMut;
pub use self::access::NodeHierarchy;
pub use self::access::NodeHierarchyMut;
pub use self::access::Nodes;
pub use self::access::NodesMut;
pub use self::access::TaskUniverse;
pub use self::access::TaskUniverseBuilder;
pub use self::access::TransformGroup;
pub use self::access::TransformGroupMut;
pub use self::access::Transforms;
pub use self::access::TransformsMut;

pub use self::dependency::Dependencies;
pub use self::dependency::Dependency;
pub use self::dependency::DependencyInner;

pub use self::graph::TaskBox;
pub use self::graph::TaskGraph;
pub use self::graph::TaskGraphBuilder;
