/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use ash::vk;

use std::fmt::Formatter;
use std::fmt::{Display, Error};
use std::time::Duration;
use std::u32;
use std::u64;

use crate::core::application::Window;
use crate::vulkan::VulkanContext;
use crate::vulkan::VulkanDevice;
use crate::vulkan::VulkanSurface;

//==================================================================================================
///
/// Assign a score of "how much we want" this particular presentation mode if we dont care about
/// screen tearing
///
fn score_present_mode_tearing(mode: vk::PresentModeKHR) -> u32 {
    match mode {
        vk::PresentModeKHR::IMMEDIATE => 4,
        vk::PresentModeKHR::MAILBOX => 3,
        vk::PresentModeKHR::FIFO_RELAXED => 2,
        vk::PresentModeKHR::FIFO => 1,
        _ => 0,
    }
}

//==================================================================================================
///
/// Assign a score of "how much we want" this particular presentation mode if we do care about
/// screen tearing
///
fn score_present_mode_vsync(mode: vk::PresentModeKHR) -> u32 {
    match mode {
        vk::PresentModeKHR::MAILBOX => 4,
        vk::PresentModeKHR::FIFO => 3,
        vk::PresentModeKHR::FIFO_RELAXED => 2,
        vk::PresentModeKHR::IMMEDIATE => 1,
        _ => 0,
    }
}

//==================================================================================================
///
/// Assign a score of "how much we want" this particular presentation mode
///
fn select_surface_format(formats: &[vk::SurfaceFormatKHR]) -> vk::SurfaceFormatKHR {
    if formats.len() == 1 && formats[0].format == vk::Format::UNDEFINED {
        let format = vk::SurfaceFormatKHR::builder()
            .format(vk::Format::B8G8R8A8_UNORM)
            .color_space(vk::ColorSpaceKHR::SRGB_NONLINEAR)
            .build();
        return format;
    }
    for format in formats.iter() {
        if format.format == vk::Format::B8G8R8A8_UNORM
            && format.color_space == vk::ColorSpaceKHR::SRGB_NONLINEAR
        {
            return *format;
        }
    }
    formats[0]
}

//==================================================================================================
///
///
///
#[derive(Copy, Clone, PartialEq, Debug)]
pub enum SwapchainAquireError {
    Timeout,
    OutOfDate,
}

//==================================================================================================
///
///
///
#[derive(Copy, Clone, Debug)]
pub struct VulkanSwapchainReport {
    pub present: vk::PresentModeKHR,
    pub format: vk::SurfaceFormatKHR,
    pub extent: vk::Extent2D,
    pub image_num: u32,
}

//==================================================================================================
impl Display for VulkanSwapchainReport {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        let width = self.extent.width;
        let height = self.extent.height;

        let err = writeln!(f, "VulkanSwapchainReport {{");
        if err.is_err() {
            return err;
        }

        let err = writeln!(f, "    present mode : {:#?},", self.present);
        if err.is_err() {
            return err;
        }

        let err = writeln!(f, "    format       : {:#?},", self.format.format);
        if err.is_err() {
            return err;
        }

        let err = writeln!(f, "    color space  : {:#?},", self.format.color_space);
        if err.is_err() {
            return err;
        }

        let err = writeln!(f, "    extent       : {}x{},", width, height);
        if err.is_err() {
            return err;
        }

        let err = writeln!(f, "    image count  : {},", self.image_num);
        if err.is_err() {
            return err;
        }

        let err = writeln!(f, "}}");
        if err.is_err() {
            return err;
        }

        Ok(())
    }
}

//==================================================================================================
///
/// Utility struct for construction a vulkan swap-chain
///
/// Will attempt to create a vulkan swap-chain to the specifications provided, but if not supported
/// it will progressively fall back until it reaches minimum viable product (FIFO, 2 images)
///
pub struct VulkanSwapchainBuilder<'dev, 'inst, 'wind, 'surf> {
    surface: &'surf VulkanSurface,
    window: &'wind Window,
    ctx: &'inst VulkanContext,
    device: &'dev VulkanDevice,
    target_image_count: u32,
    target_present_mode: vk::PresentModeKHR,
    favour_vsync: bool,
}

//==================================================================================================
impl<'dev, 'inst, 'wind, 'surf> VulkanSwapchainBuilder<'dev, 'inst, 'wind, 'surf> {
    ///
    ///
    ///
    pub fn new(
        surface: &'surf VulkanSurface,
        window: &'wind Window,
        ctx: &'inst VulkanContext,
        device: &'dev VulkanDevice,
    ) -> Self {
        VulkanSwapchainBuilder {
            surface,
            window,
            ctx,
            device,
            target_image_count: 2,
            target_present_mode: vk::PresentModeKHR::FIFO,
            favour_vsync: true,
        }
    }

    ///
    /// Safest, minimum supported swap-chain present mode and back buffer count
    ///
    /// FIFO with 2 back buffers
    ///
    pub fn compatibility(self) -> Self {
        self.fifo().double_buffer()
    }

    ///
    /// Fastest v-sync option.
    ///
    /// MAILBOX with 3 back buffers
    ///
    pub fn vsync(self) -> Self {
        self.mailbox().triple_buffer()
    }

    ///
    /// Will v-sync when above refresh rate but will allow tearing when below refresh rate
    ///
    /// FIFO_RELAXED with 3 back buffers
    ///
    pub fn fast_vsync(self) -> Self {
        self.fifo_relaxed().triple_buffer()
    }

    ///
    /// Fastest present mode and back buffer count. Will produce tearing
    ///
    /// IMMEDIATE with 2 back buffers
    ///
    pub fn fast(self) -> Self {
        self.immediate().double_buffer()
    }

    ///
    /// If the target present mode is not available then make the swapchain favour using one of the
    /// available present modes that provides the best vsync capabilities
    ///
    pub fn want_vsync(self) -> Self {
        self.vsync_affinity(true)
    }

    ///
    /// If the target present mode is not available then make the swapchain favour using a fast, low
    /// latency present mode like IMMEDIATE or MAILBOX
    ///
    pub fn want_fast(self) -> Self {
        self.vsync_affinity(false)
    }

    ///
    /// Set whether to favour vsync present modes or fast present modes
    ///
    pub fn vsync_affinity(mut self, favour_vsync: bool) -> Self {
        self.favour_vsync = favour_vsync;
        self
    }

    ///
    /// Short-form for setting back buffer count to 2
    ///
    pub fn double_buffer(self) -> Self {
        self.back_buffers(2)
    }

    ///
    /// Short-form for setting back buffer count to 3
    ///
    pub fn triple_buffer(self) -> Self {
        self.back_buffers(3)
    }

    ///
    /// Set the target number of back buffers
    ///
    pub fn back_buffers(mut self, count: u32) -> Self {
        self.target_image_count = count;
        self
    }

    ///
    /// Short-form for setting target presentation mode to FIFO
    ///
    pub fn fifo(self) -> Self {
        self.present_mode(vk::PresentModeKHR::FIFO)
    }

    ///
    /// Short-form for setting target presentation mode to FIFO
    ///
    pub fn fifo_relaxed(self) -> Self {
        self.present_mode(vk::PresentModeKHR::FIFO_RELAXED)
    }

    ///
    /// Short-form for setting target presentation mode to MAILBOX
    ///
    pub fn mailbox(self) -> Self {
        self.present_mode(vk::PresentModeKHR::MAILBOX)
    }

    ///
    /// Short-form for setting target presentation mode to IMMEDIATE
    ///
    pub fn immediate(self) -> Self {
        self.present_mode(vk::PresentModeKHR::IMMEDIATE)
    }

    ///
    /// Set the target presentation mode
    ///
    pub fn present_mode(mut self, present_mode: vk::PresentModeKHR) -> Self {
        self.target_present_mode = present_mode;
        self
    }

    ///
    /// Construct the swapchain
    ///
    pub unsafe fn build(self) -> VulkanSwapchain {
        let swap_loader = self
            .device
            .swap_loader()
            .expect("Attempting to build a swapchain without the extension loaded")
            .clone();

        let mut swap = VulkanSwapchain {
            surface: self.surface.handle(),
            swapchain: vk::SwapchainKHR::default(),
            swapchain_present: self.target_present_mode,
            swapchain_format: vk::SurfaceFormatKHR::default(),
            swapchain_extent: vk::Extent2D::default(),
            swapchain_images: Vec::new(),
            swap_loader,
            physical_device: self.device.gpu(),
            favour_vsync: self.favour_vsync,
            is_requires_rebuild: true,
        };

        swap.swapchain_images
            .resize(self.target_image_count as usize, vk::Image::default());

        if swap.rebuild(self.window, self.ctx).is_err() {
            panic!("Failed to construct swapchain");
        }

        swap
    }
}

//==================================================================================================
///
///
///
pub struct VulkanSwapchain {
    surface: vk::SurfaceKHR,
    swapchain: vk::SwapchainKHR,
    swapchain_present: vk::PresentModeKHR,
    swapchain_format: vk::SurfaceFormatKHR,
    swapchain_extent: vk::Extent2D,
    swapchain_images: Vec<vk::Image>,
    swap_loader: ash::extensions::khr::Swapchain,
    physical_device: vk::PhysicalDevice,
    favour_vsync: bool,
    is_requires_rebuild: bool,
}

//==================================================================================================
impl<'dev, 'inst, 'wind, 'surf> VulkanSwapchain {
    ///
    ///
    ///
    pub fn builder(
        surface: &'surf VulkanSurface,
        window: &'wind Window,
        ctx: &'inst VulkanContext,
        device: &'dev VulkanDevice,
    ) -> VulkanSwapchainBuilder<'dev, 'inst, 'wind, 'surf> {
        VulkanSwapchainBuilder::new(surface, window, ctx, device)
    }

    ///
    /// Get the Surface handle
    ///
    pub fn surface(&self) -> vk::SurfaceKHR {
        self.surface
    }

    ///
    /// Get the Swapchain handle
    ///
    pub fn swapchain(&self) -> vk::SwapchainKHR {
        self.swapchain
    }

    ///
    /// Get the PresentMode
    ///
    pub fn present_mode(&self) -> vk::PresentModeKHR {
        self.swapchain_present
    }

    ///
    /// Get the SurfaceFormat
    ///
    pub fn format(&self) -> &vk::SurfaceFormatKHR {
        &self.swapchain_format
    }

    ///
    /// Get the extent
    ///
    pub fn extent(&self) -> &vk::Extent2D {
        &self.swapchain_extent
    }

    ///
    /// Get the surface capabilities
    ///
    pub fn capabilities(&self, context: &VulkanContext) -> vk::SurfaceCapabilitiesKHR {
        unsafe {
            let loader = context.surface_loader().unwrap();

            let capabilities = loader
                .get_physical_device_surface_capabilities(self.physical_device, self.surface)
                .unwrap();
            capabilities
        }
    }

    ///
    /// Get the supported surface formats
    ///
    pub fn supported_formats(&self, context: &VulkanContext) -> Vec<vk::SurfaceFormatKHR> {
        unsafe {
            let loader = context.surface_loader().unwrap();

            loader
                .get_physical_device_surface_formats(self.physical_device, self.surface)
                .unwrap()
        }
    }

    pub fn supported_present_modes(&self, context: &VulkanContext) -> Vec<vk::PresentModeKHR> {
        unsafe {
            let loader = context.surface_loader().unwrap();

            loader
                .get_physical_device_surface_present_modes(self.physical_device, self.surface)
                .unwrap()
        }
    }

    ///
    /// Get the aspect ratio
    ///
    pub fn aspect(&self) -> f32 {
        self.swapchain_extent.width as f32 / self.swapchain_extent.height as f32
    }

    /// Get a reference to the array of Image handles
    pub fn images(&self) -> &[vk::Image] {
        &self.swapchain_images
    }

    ///
    /// Cleanup the internal handles and close the swapchain
    ///
    pub unsafe fn destroy(&mut self) {
        self.swap_loader.destroy_swapchain(self.swapchain, None);

        self.swapchain = vk::SwapchainKHR::default();
    }

    ///
    ///
    ///
    pub fn report(&self) -> VulkanSwapchainReport {
        VulkanSwapchainReport {
            present: self.swapchain_present,
            format: self.swapchain_format,
            extent: self.swapchain_extent,
            image_num: self.swapchain_images.len() as u32,
        }
    }

    ///
    ///
    ///
    pub fn acquire_next(
        &mut self,
        timeout: Duration,
        semaphore: vk::Semaphore,
        fence: vk::Fence,
    ) -> Result<(u32, vk::Image), SwapchainAquireError> {
        if self.is_requires_rebuild {
            return Err(SwapchainAquireError::OutOfDate);
        }

        let timeout = timeout.as_nanos() as u64;

        let result = unsafe {
            self.swap_loader
                .acquire_next_image(self.swapchain, timeout, semaphore, fence)
        };
        match result {
            Ok((index, suboptimal)) => {
                if suboptimal {
                    self.is_requires_rebuild = true;
                }
                Ok((index, self.swapchain_images[index as usize]))
            }
            Err(err) => {
                match err {
                    vk::Result::ERROR_OUT_OF_DATE_KHR => {
                        // Rebuilding can fail so we need to make sure it keeps trying until it
                        // succeeds
                        self.is_requires_rebuild = true;
                        Err(SwapchainAquireError::OutOfDate)
                    }
                    vk::Result::TIMEOUT => Err(SwapchainAquireError::Timeout),
                    _ => {
                        panic!(
                            "We've lost the swapchain. Probably not worth trying to recover\n\
                             ERROR: {:#?}",
                            err
                        );
                    }
                }
            }
        }
    }

    ///
    ///
    ///
    pub unsafe fn present(&mut self, queue: vk::Queue, index: usize, semaphores: &[vk::Semaphore]) {
        let swapchains = [self.swapchain];
        let indices = [index as u32];

        let present_info = vk::PresentInfoKHR::builder()
            .swapchains(&swapchains)
            .wait_semaphores(semaphores)
            .image_indices(&indices)
            .build();
        match self.swap_loader.queue_present(queue, &present_info) {
            Ok(suboptimal) => {
                if suboptimal {
                    self.is_requires_rebuild = true;
                }
            }
            Err(result) => {
                if result == vk::Result::ERROR_OUT_OF_DATE_KHR {
                    self.is_requires_rebuild = true;
                }
            }
        }
    }

    ///
    ///
    ///
    pub fn is_requires_rebuild(&self) -> bool {
        self.is_requires_rebuild
    }

    ///
    ///
    ///
    pub fn queue_rebuild(&mut self) {
        self.is_requires_rebuild = true;
    }

    ///
    ///
    ///
    pub unsafe fn rebuild(&mut self, window: &Window, ctx: &VulkanContext) -> Result<(), ()> {
        let capabilities = self.capabilities(ctx);

        if capabilities.current_extent.width == 0 || capabilities.current_extent.height == 0 {
            return Err(());
        }

        if self.swapchain != vk::SwapchainKHR::default() {
            self.swap_loader.destroy_swapchain(self.swapchain, None);
        }

        let swapchain_extent = if capabilities.current_extent.width != u32::MAX {
            capabilities.current_extent
        } else {
            let (w, h) = window.drawable_dimensions();
            let mut actual = vk::Extent2D::default();
            actual.width = w;
            actual.height = h;

            let width = capabilities.max_image_extent.width.min(actual.width);
            let height = capabilities.max_image_extent.height.min(actual.height);

            actual.width = capabilities.min_image_extent.width.max(width);
            actual.height = capabilities.min_image_extent.height.max(height);

            actual
        };

        let swapchain_present = {
            let modes = self.supported_present_modes(ctx);

            let mut best_score: u32 = 0;
            let mut best_mode = vk::PresentModeKHR::default();
            for mode in modes.iter() {
                let score = if self.favour_vsync {
                    score_present_mode_vsync(*mode)
                } else {
                    score_present_mode_tearing(*mode)
                };

                if *mode == self.swapchain_present {
                    best_score = score;
                    best_mode = self.swapchain_present;
                    break;
                } else if score > best_score {
                    best_score = score;
                    best_mode = *mode;
                }
            }
            if best_score == 0 {
                panic!("Failed to find a usable presentation mode");
            }
            best_mode
        };

        let formats = self.supported_formats(ctx);
        let swapchain_format = select_surface_format(&formats);

        let image_count = {
            if (self.swapchain_images.len() as u32) < capabilities.min_image_count {
                capabilities.min_image_count
            } else {
                self.swapchain_images.len() as u32
            }
        };

        let swap_create_info = vk::SwapchainCreateInfoKHR::builder()
            .surface(self.surface)
            .min_image_count(image_count)
            .present_mode(swapchain_present)
            .image_format(swapchain_format.format)
            .image_color_space(swapchain_format.color_space)
            .image_extent(swapchain_extent)
            .image_array_layers(1)
            .image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT)
            .pre_transform(capabilities.current_transform)
            .composite_alpha(vk::CompositeAlphaFlagsKHR::OPAQUE)
            .image_sharing_mode(vk::SharingMode::EXCLUSIVE)
            .clipped(true)
            .build();

        let swapchain = self
            .swap_loader
            .create_swapchain(&swap_create_info, None)
            .expect("Failed to create swapchain");

        let swapchain_images = self
            .swap_loader
            .get_swapchain_images(swapchain)
            .expect("Failed to retrieve swapchain images");

        self.swapchain_format = swapchain_format;
        self.swapchain_extent = swapchain_extent;
        self.swapchain_present = swapchain_present;
        self.swapchain_images = swapchain_images;
        self.swapchain = swapchain;
        self.is_requires_rebuild = false;

        Ok(())
    }
}

//==================================================================================================
impl<'dev> Drop for VulkanSwapchain {
    fn drop(&mut self) {
        if self.swapchain != vk::SwapchainKHR::default() {
            panic!("VulkanSwapchain dropped without being destroyed");
        }
    }
}
