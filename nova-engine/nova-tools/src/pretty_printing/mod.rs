/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use console::style;

pub fn print_title(title: &str) {
    let mut header_footer = String::from("|");
    for _ in 0..title.len() + 4 {
        header_footer.push('-')
    }
    header_footer.push('|');

    println!(
        "{}\n|  {}  |\n{}",
        &header_footer,
        style(title).green().bold(),
        &header_footer
    );
}

pub fn print_subtitle(subtitle: &str) {
    println!("[{}]", style(subtitle).green());
}

pub fn print_file_copy(name: &std::path::Path) {
    let name_text = style(name).bold().cyan();
    println!("| Copying File: {:?}", name_text);
}

pub fn print_file_write(name: &std::path::Path) {
    let name_text = style(name).bold().cyan();
    println!("| Writing File: {:?}", name_text);
}

pub fn print_dir_create(name: &std::path::Path) {
    let name_text = style(name).bold().cyan();
    println!("| Creating Directory: {:?}", name_text);
}
