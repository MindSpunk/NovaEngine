/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

///
/// FMOD_STUDIO_PLAYBACK_STATE
///
#[repr(u32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum PlaybackState {
    Playing = 0,
    Sustaining = 1,
    Stopped = 2,
    Starting = 3,
    Stopping = 4,
}

impl PlaybackState {
    pub fn from_ffi(ffi: raw::FMOD_STUDIO_PLAYBACK_STATE) -> Self {
        match ffi {
            raw::FMOD_STUDIO_PLAYBACK_STATE::FMOD_STUDIO_PLAYBACK_PLAYING => PlaybackState::Playing,
            raw::FMOD_STUDIO_PLAYBACK_STATE::FMOD_STUDIO_PLAYBACK_SUSTAINING => {
                PlaybackState::Sustaining
            }
            raw::FMOD_STUDIO_PLAYBACK_STATE::FMOD_STUDIO_PLAYBACK_STOPPED => PlaybackState::Stopped,
            raw::FMOD_STUDIO_PLAYBACK_STATE::FMOD_STUDIO_PLAYBACK_STARTING => {
                PlaybackState::Starting
            }
            raw::FMOD_STUDIO_PLAYBACK_STATE::FMOD_STUDIO_PLAYBACK_STOPPING => {
                PlaybackState::Stopping
            }
            raw::FMOD_STUDIO_PLAYBACK_STATE::FMOD_STUDIO_PLAYBACK_FORCEINT => unreachable!(),
        }
    }

    pub fn into_ffi(self) -> raw::FMOD_STUDIO_PLAYBACK_STATE {
        match self {
            PlaybackState::Playing => raw::FMOD_STUDIO_PLAYBACK_STATE::FMOD_STUDIO_PLAYBACK_PLAYING,
            PlaybackState::Sustaining => {
                raw::FMOD_STUDIO_PLAYBACK_STATE::FMOD_STUDIO_PLAYBACK_SUSTAINING
            }
            PlaybackState::Stopped => raw::FMOD_STUDIO_PLAYBACK_STATE::FMOD_STUDIO_PLAYBACK_STOPPED,
            PlaybackState::Starting => {
                raw::FMOD_STUDIO_PLAYBACK_STATE::FMOD_STUDIO_PLAYBACK_STARTING
            }
            PlaybackState::Stopping => {
                raw::FMOD_STUDIO_PLAYBACK_STATE::FMOD_STUDIO_PLAYBACK_STOPPING
            }
        }
    }
}
