/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

mod camera;
mod debug_camera;
mod spatial;
mod static_mesh;

pub use camera::Camera;
pub use debug_camera::DebugCamera;
pub use spatial::Spatial;
pub use static_mesh::StaticMesh;

use crate::universe::Universe;

pub fn registed_default_nodes(universe: &mut Universe) {
    // Probably won't need many of these so preallocate for just a few
    universe.register_node_type::<Camera>(16);

    // We'll definitely only need few of these (only 1) but we'll preallocate for more just in case
    universe.register_node_type::<DebugCamera>(4);

    // We'll need a lot of these so we'll preallocate more memory for them
    universe.register_node_type::<StaticMesh>(128);
    universe.register_node_type::<Spatial>(128);
}
