/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use memmap::{Mmap, MmapOptions};

use std::collections::hash_map::DefaultHasher;
use std::hash::Hasher;
use std::io::{Cursor, Error, ErrorKind, Read, Result};
use std::marker::PhantomData;

use core::ptr;

use crate::constants::{FileFlags, HEADER_MAGIC, VERSION_0_5_0};
use crate::{raw, DirIter};

// =================================================================================================
///
/// A file handle's underlying data
///
#[derive(Copy, Clone)]
pub struct File<'archive> {
    name_hash: u64,
    name: &'archive str,
    data: &'archive [u8],
    flags: FileFlags,
}

impl<'archive> File<'archive> {
    ///
    ///
    ///
    pub(crate) fn new(
        name_hash: u64,
        name: &'archive str,
        flags: FileFlags,
        data: &'archive [u8],
    ) -> Self {
        Self {
            name_hash,
            name,
            data,
            flags,
        }
    }

    ///
    ///
    ///
    #[inline]
    pub fn name(&self) -> &str {
        self.name

        //let len = unsafe { *(self.name_ptr as *const usize) };
        //let data = unsafe { self.name_ptr.add(8) };
        //self.archive.assert_blob_ptr(unsafe { data.add(len) });
        //let name_slice = unsafe { std::slice::from_raw_parts(data, len) };
        //std::str::from_utf8(name_slice).unwrap()
    }

    ///
    ///
    ///
    #[inline]
    pub fn name_hash(&self) -> u64 {
        self.name_hash
    }

    ///
    /// Get a slice of the file data if the file is stored uncompressed
    ///
    pub fn data(&self) -> Option<&'archive [u8]> {
        if self.flags.contains(FileFlags::UNCOMPRESSED) {
            Some(self.data)
        } else {
            None
        }
    }

    ///
    /// Gets a reader for the data
    ///
    pub fn reader(&self) -> Box<dyn Read + 'archive> {
        if self.flags.contains(FileFlags::UNCOMPRESSED) {
            let bytes = Cursor::new(self.data);
            Box::new(bytes)
        } else if self.flags.contains(FileFlags::COMPRESSED_ZLIB) {
            let decoder = flate2::read::ZlibDecoder::new(self.data);
            Box::new(decoder)
        } else {
            panic!("Tried to get a reader from an invalid archive file");
        }
    }
}

// =================================================================================================
///
/// A folder handle's underlying data
///
#[derive(Copy, Clone)]
pub struct Folder<'archive, 'file> {
    archive: &'archive Archive<'file>,
    name_hash: u64,
    name: &'archive str,
    dir: *const raw::DirRaw,
    phantom: PhantomData<&'archive raw::DirRaw>,
}

impl<'archive, 'file> Folder<'archive, 'file> {
    ///
    ///
    ///
    pub(crate) fn new(
        archive: &'archive Archive<'file>,
        name_hash: u64,
        name: &'archive str,
        dir: *const raw::DirRaw,
    ) -> Self {
        Self {
            archive,
            name_hash,
            name,
            dir,
            phantom: Default::default(),
        }
    }

    ///
    ///
    ///
    #[inline]
    pub fn name(&self) -> &str {
        self.name
    }

    ///
    ///
    ///
    #[inline]
    pub fn name_hash(&self) -> u64 {
        self.name_hash
    }

    ///
    /// Gets a DirIter over this folder's children
    ///
    pub fn children(&self) -> DirIter<'archive, 'file> {
        unsafe {
            let next = (self.dir as *const raw::DirRaw).add(1);
            let next = &*next;
            let iterator = DirIter {
                archive: self.archive,
                child_count: (&*self.dir).child_count as usize,
                next,
            };
            iterator
        }
    }
}

// =================================================================================================
///
///
///
#[derive(Copy, Clone)]
pub enum Entry<'archive, 'file> {
    File(File<'archive>),
    Folder(Folder<'archive, 'file>),
}

impl<'archive, 'file> Entry<'archive, 'file> {
    ///
    ///
    ///
    #[inline]
    pub fn name(&self) -> &str {
        match self {
            Entry::File(file) => file.name(),
            Entry::Folder(folder) => folder.name(),
        }
    }

    ///
    ///
    ///
    #[inline]
    pub fn name_hash(&self) -> u64 {
        match self {
            Entry::File(file) => file.name_hash(),
            Entry::Folder(folder) => folder.name_hash(),
        }
    }

    ///
    ///
    ///
    pub fn is_file(&self) -> bool {
        match self {
            Entry::File(_) => true,
            Entry::Folder(_) => false,
        }
    }

    ///
    ///
    ///
    pub fn is_folder(&self) -> bool {
        match self {
            Entry::File(_) => false,
            Entry::Folder(_) => true,
        }
    }

    ///
    /// Get a slice of the file data if the file is stored uncompressed
    ///
    pub fn file_data(&self) -> Option<&[u8]> {
        match self {
            Entry::File(file) => file.data(),
            Entry::Folder(_) => None,
        }
    }

    ///
    ///
    ///
    pub fn reader(&self) -> Option<Box<dyn Read + 'archive>> {
        match self {
            Entry::File(file) => Some(file.reader()),
            Entry::Folder(_) => None,
        }
    }

    ///
    ///
    ///
    pub fn children(&self) -> Option<DirIter<'archive, 'file>> {
        match self {
            Entry::File(_) => None,
            Entry::Folder(folder) => Some(folder.children()),
        }
    }
}

pub struct Archive<'file> {
    file: PhantomData<&'file ()>,
    memmap: Mmap,
    ent_section: *const u8,
    dir_section: *const u8,
}

impl<'file> Archive<'file> {
    ///
    /// Opens an archive
    ///
    pub fn open(file: &'file std::fs::File) -> Result<Self> {
        let memmap = unsafe { MmapOptions::default().map(file)? };
        let mut out = Self {
            file: Default::default(),
            memmap,
            ent_section: ptr::null(),
            dir_section: ptr::null(),
        };

        // Check if the file is too short for even the file header to fit
        if out.len() < 512 {
            return Err(Error::new(ErrorKind::InvalidData, "File too short"));
        }

        // Get the header
        let header = out.base_ptr() as *const raw::HeaderRaw;
        let header = unsafe { &*header };

        // Check if we have a valid magic number at the start
        if header.magic != HEADER_MAGIC {
            return Err(Error::new(ErrorKind::InvalidData, "Not an archive file"));
        }

        // Check if we can read this version of the file
        if header.version < VERSION_0_5_0 || header.version > VERSION_0_5_0 {
            return Err(Error::new(ErrorKind::InvalidData, "Invalid file version"));
        }

        // Calculate actual pointers in process address space for entry and dir section
        out.ent_section = out.calc_ptr(header.ent_ptr as *const u8);
        out.dir_section = out.calc_ptr(header.dir_ptr as *const u8);

        // Get a slice of the blob section
        let blob_slice = {
            let ptr = out.calc_ptr(512 as *const u8);
            let len = unsafe { out.entry_ptr().sub(ptr as usize) } as usize;
            unsafe { std::slice::from_raw_parts(ptr, len) }
        };

        // Hash the blob section and verify if it matches the hash in the header
        let mut hasher = DefaultHasher::new();
        hasher.write(blob_slice);
        if hasher.finish() != header.blob_hash {
            return Err(Error::new(
                ErrorKind::InvalidData,
                "Blob hash does not match data",
            ));
        }

        Ok(out)
    }

    ///
    /// Returns an entry handle to the root directory
    ///
    pub fn root(&self) -> Entry<'_, 'file> {
        let ptr = self.assert_dir_ptr(self.dir_ptr() as *const raw::DirRaw);
        let dir = unsafe { &*ptr };

        let entries = self.entries_slice();
        let entry_raw = &entries[dir.index as usize];

        let name = self.calc_slice(entry_raw.string_ptr as *const u8);
        let name = std::str::from_utf8(name).unwrap();

        let name_hash = entry_raw.string_hash;

        let folder = Folder::new(self, name_hash, name, dir);

        Entry::Folder(folder)
    }

    ///
    ///
    ///
    pub fn lookup(&self, path: &str) -> Option<Entry> {
        if path.starts_with('/') {
            let path = path.split_at(1).1;

            let mut entry = self.root();

            for name in path.split('/') {
                let mut hasher = DefaultHasher::new();
                hasher.write(name.as_bytes());
                let hash = hasher.finish();

                let mut found = false;
                for child in entry.children()? {
                    if child.name_hash() == hash {
                        entry = child;
                        found = true;
                        break;
                    }
                }

                if !found {
                    return None;
                }
            }

            Some(entry)
        } else {
            None
        }
    }

    ///
    /// Gets the size of the memory map in bytes
    ///
    #[inline]
    pub fn len(&self) -> usize {
        self.memmap.len()
    }

    ///
    /// Returns the size of the blob section in bytes
    ///
    #[inline]
    pub fn blob_len(&self) -> usize {
        let blob = self.blob_ptr() as usize;
        let ent = self.entry_ptr() as usize;
        ent - blob
    }

    ///
    /// Returns the size of the directory section in bytes
    ///
    pub fn entry_len(&self) -> usize {
        let ent = self.entry_ptr() as usize;
        let dir = self.dir_ptr() as usize;
        dir - ent
    }

    ///
    /// Returns the size of the directory section in bytes
    ///
    pub fn dir_len(&self) -> usize {
        let dir = self.dir_ptr() as usize;
        let end = self.end_ptr() as usize;
        end - dir
    }

    ///
    /// Checks if a given pointer is a valid pointer to the blob section
    ///
    #[inline]
    pub(crate) fn ptr_in_blob_section<T>(&self, ptr: *const T) -> bool {
        let ptr = ptr as *const u8;
        ptr >= self.blob_ptr() && ptr < self.entry_ptr()
    }

    ///
    /// Checks if a given pointer is a valid pointer to the entry section
    ///
    #[inline]
    pub(crate) fn ptr_in_entry_section<T>(&self, ptr: *const T) -> bool {
        let ptr = ptr as *const u8;
        ptr >= self.entry_ptr() && ptr < self.dir_ptr()
    }

    ///
    /// Checks if a given pointer is a valid pointer to the directory section
    ///
    #[inline]
    pub(crate) fn ptr_in_dir_section<T>(&self, ptr: *const T) -> bool {
        let ptr = ptr as *const u8;
        ptr >= self.dir_ptr() && ptr < self.end_ptr()
    }

    ///
    /// Returns the pointer if it is a valid pointer to the blob section or panics if it isn't
    ///
    #[inline]
    pub(crate) fn assert_blob_ptr<T>(&self, ptr: *const T) -> *const T {
        if self.ptr_in_blob_section(ptr) {
            ptr
        } else {
            panic!("Invalid pointer to blob section");
        }
    }

    ///
    /// Returns the pointer if it is a valid pointer to the entry section or panics if it isn't
    ///
    #[inline]
    pub(crate) fn assert_entry_ptr<T>(&self, ptr: *const T) -> *const T {
        if self.ptr_in_entry_section(ptr) {
            ptr
        } else {
            panic!("Invalid pointer to entry section");
        }
    }

    ///
    /// Returns the pointer if it is a valid pointer to the directory section or panics if it isn't
    ///
    #[inline]
    pub(crate) fn assert_dir_ptr<T>(&self, ptr: *const T) -> *const T {
        if self.ptr_in_dir_section(ptr) {
            ptr
        } else {
            panic!("Invalid pointer to directory section");
        }
    }

    ///
    /// Gets the base pointer of the memory map
    ///
    #[inline]
    pub(crate) fn base_ptr(&self) -> *const u8 {
        self.memmap.as_ptr()
    }

    ///
    /// Gets a pointer to the blob section in the memory map
    ///
    #[inline]
    pub(crate) fn blob_ptr(&self) -> *const u8 {
        unsafe { self.base_ptr().add(512) }
    }

    ///
    /// Gets a pointer to the entry section in the memory map
    ///
    #[inline]
    pub(crate) fn entry_ptr(&self) -> *const u8 {
        self.ent_section
    }

    ///
    /// Gets a pointer to the directory section in the memory map
    ///
    #[inline]
    pub(crate) fn dir_ptr(&self) -> *const u8 {
        self.dir_section
    }

    ///
    /// Gets a pointer to the end of the memory map
    ///
    #[inline]
    pub(crate) fn end_ptr(&self) -> *const u8 {
        unsafe { self.memmap.as_ptr().add(self.memmap.len()) }
    }

    ///
    /// Gets a slice that holds all file entries in the archive
    ///
    #[inline]
    pub(crate) fn entries_slice(&self) -> &[raw::EntryRaw] {
        let section = self.entry_ptr() as *const u64;
        let section = self.assert_entry_ptr(section);

        // First part of the entries section is the number of entries
        let size = unsafe { *section } as usize;

        // Data starts right after
        let section = unsafe { section.add(1) } as *const raw::EntryRaw;

        unsafe { std::slice::from_raw_parts(section, size) }
    }

    ///
    /// Converts a file pointer to a real pointer relative to the base of the memory map
    ///
    #[inline]
    pub(crate) fn calc_ptr<T>(&self, offset: *const T) -> *const T {
        unsafe { self.base_ptr().add(offset as usize) as *const T }
    }

    #[inline]
    pub(crate) fn calc_slice<T>(&self, ptr: *const T) -> &[T] {
        unsafe {
            let ptr = self.calc_ptr(ptr);
            let len = *(ptr as *const usize);
            let data = (ptr as *const u8).add(8) as *const T;
            self.assert_blob_ptr(data.add(len));
            std::slice::from_raw_parts(data, len)
        }
    }
}
