/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

// TODO: Copy SDL2 artifacts to jniLibs folder
// TODO: Copy libvma to output folder

use crate::pretty_printing::print_dir_create;
use crate::pretty_printing::print_file_write;
use crate::pretty_printing::print_subtitle;
use crate::pretty_printing::print_title;
use crate::traits::Process;
use clap::{App, Arg, ArgMatches, SubCommand};
use serde::Deserialize;
use serde::Serialize;

pub const ANDROID_GEN_NAME: &str = "android-project-gen";
pub const ANDROID_GEN_OUTPUT_NAME: &str = "out-dir";
pub const ANDROID_GEN_OUTPUT_NAME_SHORT: &str = "o";

const DEFAULT_APPLICATION_ID: &str = "org.nova.game";
const DEFAULT_VERSION_NAME: &str = "0.1";
const DEFAULT_APP_NAME: &str = "NovaGame";
const DEFAULT_SHARED_OBJECT: &str = "nova_test_android";
const DEFAULT_OUT_DIR: &str = "ANDROID";

pub struct AndroidProjectGenerator {}

impl AndroidProjectGenerator {
    pub const COMMAND_NAME: &'static str = ANDROID_GEN_NAME;

    pub fn new() -> Self {
        Self {}
    }
}

impl Process for AndroidProjectGenerator {
    fn args<'a, 'b>(&mut self, app: App<'a, 'b>) -> App<'a, 'b> {
        let output_arg = Arg::with_name(ANDROID_GEN_OUTPUT_NAME)
            .long(ANDROID_GEN_OUTPUT_NAME)
            .short(ANDROID_GEN_OUTPUT_NAME_SHORT)
            .help("Sets the output directory where the project will be placed")
            .value_name("OUTPUT")
            .takes_value(true);

        let sub_command = SubCommand::with_name(ANDROID_GEN_NAME)
            .about("Generates an android project for building for android")
            .arg(output_arg);

        app.subcommand(sub_command)
    }

    fn exec(&mut self, matches: &ArgMatches) {
        fn write_file(bytes: &[u8], dest: &std::path::Path) {
            print_file_write(dest);
            std::fs::write(dest, bytes).unwrap();
        }

        print_title("Generating Android Project");

        //
        // Load the template files into the handlebars context
        //
        let mut registry = handlebars::Handlebars::new();
        register_templates(&mut registry);

        print_subtitle("Creating Directory Structure");

        //
        // Get output directory from command line or default
        //
        let out_dir_root = matches
            .value_of(ANDROID_GEN_OUTPUT_NAME)
            .unwrap_or(DEFAULT_OUT_DIR);
        let out_dir_root = std::path::Path::new(out_dir_root);

        //
        // Stamp out the file structure
        //
        create_file_structure(out_dir_root);

        //
        // Copy all non template files to their expected place
        //
        print_subtitle("Creating Files");

        //
        // Write out the root gradle files
        //
        let build_gradle = include_bytes!("../../../templates/android-project/build.gradle");
        let gradle_properties =
            include_bytes!("../../../templates/android-project/gradle.properties");
        let gradlew = include_bytes!("../../../templates/android-project/gradlew");
        let gradlew_bat = include_bytes!("../../../templates/android-project/gradlew.bat");
        let settings_gradle = include_bytes!("../../../templates/android-project/settings.gradle");
        let templated_files_txt =
            include_bytes!("../../../templates/android-project/templated_files.txt");

        write_file(build_gradle, &out_dir_root.join("build.grade"));
        write_file(gradle_properties, &out_dir_root.join("gradle.properties"));
        write_file(gradlew, &out_dir_root.join("gradlew"));
        write_file(gradlew_bat, &out_dir_root.join("gradlew.bat"));
        write_file(settings_gradle, &out_dir_root.join("settings.gradle"));
        write_file(
            templated_files_txt,
            &out_dir_root.join("templated_files.txt"),
        );

        //
        // Write out the gradle wrapper files
        //
        let gradle_wrapper_jar =
            include_bytes!("../../../templates/android-project/gradle/wrapper/gradle-wrapper.jar");
        let gradle_wrapper_properties = include_bytes!(
            "../../../templates/android-project/gradle/wrapper/gradle-wrapper.properties"
        );

        let out_dir = out_dir_root.join("gradle/wrapper");

        write_file(gradle_wrapper_jar, &out_dir.join("gradle-wrapper.jar"));
        write_file(
            gradle_wrapper_properties,
            &out_dir.join("gradle-wrapper.properties"),
        );

        //
        // Write out the app gradle files
        //
        let build_gradle = include_bytes!("../../../templates/android-project/app/build.gradle");
        let proguard_rules_pro =
            include_bytes!("../../../templates/android-project/app/proguard-rules.pro");

        let out_dir = out_dir_root.join("app");

        write_file(build_gradle, &out_dir.join("build.gradle"));
        write_file(proguard_rules_pro, &out_dir.join("proguard-rules.pro"));

        //
        // Write out the jni makefiles
        //
        let android_mk =
            include_bytes!("../../../templates/android-project/app/src/main/jni/Android.mk");
        let application_mk =
            include_bytes!("../../../templates/android-project/app/src/main/jni/Application.mk");

        let out_dir = out_dir_root.join("app/src/main/jni");

        write_file(android_mk, &out_dir.join("Android.mk"));
        write_file(application_mk, &out_dir.join("Application.mk"));

        //
        // Write out the app resource files
        //
        let hdpi_ic_launcher = include_bytes!(
            "../../../templates/android-project/app/src/main/res/mipmap-hdpi/ic_launcher.png"
        );
        let mdpi_ic_launcher = include_bytes!(
            "../../../templates/android-project/app/src/main/res/mipmap-mdpi/ic_launcher.png"
        );
        let xdpi_ic_launcher = include_bytes!(
            "../../../templates/android-project/app/src/main/res/mipmap-xhdpi/ic_launcher.png"
        );
        let xxdpi_ic_launcher = include_bytes!(
            "../../../templates/android-project/app/src/main/res/mipmap-xxhdpi/ic_launcher.png"
        );
        let xxxdpi_ic_launcher = include_bytes!(
            "../../../templates/android-project/app/src/main/res/mipmap-xxxhdpi/ic_launcher.png"
        );

        let colors_xml =
            include_bytes!("../../../templates/android-project/app/src/main/res/values/colors.xml");
        let styles_xml =
            include_bytes!("../../../templates/android-project/app/src/main/res/values/styles.xml");

        let out_dir = out_dir_root.join("app/src/main/res");

        write_file(
            hdpi_ic_launcher,
            &out_dir.join("mipmap-hdpi/ic_launcher.png"),
        );
        write_file(
            mdpi_ic_launcher,
            &out_dir.join("mipmap-mdpi/ic_launcher.png"),
        );
        write_file(
            xdpi_ic_launcher,
            &out_dir.join("mipmap-xhdpi/ic_launcher.png"),
        );
        write_file(
            xxdpi_ic_launcher,
            &out_dir.join("mipmap-xxhdpi/ic_launcher.png"),
        );
        write_file(
            xxxdpi_ic_launcher,
            &out_dir.join("mipmap-xxxhdpi/ic_launcher.png"),
        );

        write_file(colors_xml, &out_dir.join("values/colors.xml"));
        write_file(styles_xml, &out_dir.join("values/styles.xml"));

        //
        // Write out the SDL java glue
        //
        let hid_device = include_bytes!(
            "../../../templates/android-project/app/src/main/java/sdl/HIDDevice.java"
        );
        let hid_device_ble_stream_controller = include_bytes!("../../../templates/android-project/app/src/main/java/sdl/HIDDeviceBLESteamController.java");
        let hid_device_manager = include_bytes!(
            "../../../templates/android-project/app/src/main/java/sdl/HIDDeviceManager.java"
        );
        let hid_device_usb = include_bytes!(
            "../../../templates/android-project/app/src/main/java/sdl/HIDDeviceUSB.java"
        );
        let sdl =
            include_bytes!("../../../templates/android-project/app/src/main/java/sdl/SDL.java");
        let sdl_activity = include_bytes!(
            "../../../templates/android-project/app/src/main/java/sdl/SDLActivity.java"
        );
        let sdl_audio_manager = include_bytes!(
            "../../../templates/android-project/app/src/main/java/sdl/SDLAudioManager.java"
        );
        let sdl_controller_manager = include_bytes!(
            "../../../templates/android-project/app/src/main/java/sdl/SDLControllerManager.java"
        );

        let out_dir = out_dir_root.join("app/src/main/java/org/libsdl/app");

        write_file(hid_device, &out_dir.join("HIDDevice.java"));
        write_file(
            hid_device_ble_stream_controller,
            &out_dir.join("HIDDeviceBLEStreamController.java"),
        );
        write_file(hid_device_manager, &out_dir.join("HIDDeviceManager.java"));
        write_file(hid_device_usb, &out_dir.join("HIDDeviceUSB.java"));
        write_file(sdl, &out_dir.join("SDL.java"));
        write_file(sdl_activity, &out_dir.join("SDLActivity.java"));
        write_file(sdl_audio_manager, &out_dir.join("SDLAudioManager.java"));
        write_file(
            sdl_controller_manager,
            &out_dir.join("SDLControllerManager.java"),
        );

        print_subtitle("Generating Template Files");

        ///
        /// Values that are used in the template
        ///
        #[derive(Clone, Serialize, Deserialize)]
        struct TemplateContext {
            application_id: String,
            version_name: String,
            app_name: String,
            shared_object_file: String,
        }

        let context = TemplateContext {
            application_id: DEFAULT_APPLICATION_ID.to_string(),
            version_name: DEFAULT_VERSION_NAME.to_string(),
            app_name: DEFAULT_APP_NAME.to_string(),
            shared_object_file: DEFAULT_SHARED_OBJECT.to_string(),
        };

        //
        // Stamp out the build.gradle file
        //
        {
            let gradle_template = registry.render("gradle", &context).unwrap();

            let out_dir = out_dir_root.join("app/build.gradle");

            print_file_write(&out_dir);
            std::fs::write(&out_dir, &gradle_template).unwrap();
        }

        //
        // Stamp out the AndroidManifest.xml
        //
        {
            let manifest_template = registry.render("manifest", &context).unwrap();

            let out_dir = out_dir_root.join("app/src/main/AndroidManifest.xml");

            print_file_write(&out_dir);
            std::fs::write(&out_dir, &manifest_template).unwrap();
        }

        //
        // Stamp out the strings.xml file
        //
        {
            let strings_template = registry.render("strings", &context).unwrap();

            let out_dir = out_dir_root.join("app/src/main/res/values/strings.xml");

            print_file_write(&out_dir);
            std::fs::write(&out_dir, &strings_template).unwrap();
        }

        //
        // Stamp out the NovaGameActivity.java file
        //
        {
            let package_path: std::path::PathBuf = DEFAULT_APPLICATION_ID.split('.').collect();

            let activity_template = registry.render("activity", &context).unwrap();

            let out_dir = out_dir_root.join("app/src/main/java");
            let out_dir = out_dir.join(&package_path);
            let out_dir = out_dir.join("NovaGameActivity.java");

            print_file_write(&out_dir);
            std::fs::write(&out_dir, &activity_template).unwrap();
        }
    }
}

fn create_file_structure(out_dir_root: &std::path::Path) {
    if out_dir_root.exists() {
        std::fs::remove_dir_all(out_dir_root).unwrap();
    }

    fn create_dir(path: &std::path::Path) {
        print_dir_create(path);
        std::fs::create_dir(path).unwrap();
    }

    create_dir(out_dir_root);
    create_dir(&out_dir_root.join("app"));
    create_dir(&out_dir_root.join("app/src"));
    create_dir(&out_dir_root.join("app/src/main"));
    create_dir(&out_dir_root.join("app/src/main/java"));
    create_dir(&out_dir_root.join("app/src/main/java/org"));
    create_dir(&out_dir_root.join("app/src/main/java/org/libsdl"));
    create_dir(&out_dir_root.join("app/src/main/java/org/libsdl/app"));
    create_dir(&out_dir_root.join("app/src/main/jni"));
    create_dir(&out_dir_root.join("app/src/main/jniLibs"));
    create_dir(&out_dir_root.join("app/src/main/jniLibs/x86_64"));
    create_dir(&out_dir_root.join("app/src/main/jniLibs/arm64-v8a"));
    create_dir(&out_dir_root.join("app/src/main/res"));
    create_dir(&out_dir_root.join("app/src/main/res/mipmap-hdpi"));
    create_dir(&out_dir_root.join("app/src/main/res/mipmap-mdpi"));
    create_dir(&out_dir_root.join("app/src/main/res/mipmap-xhdpi"));
    create_dir(&out_dir_root.join("app/src/main/res/mipmap-xxhdpi"));
    create_dir(&out_dir_root.join("app/src/main/res/mipmap-xxxhdpi"));
    create_dir(&out_dir_root.join("app/src/main/res/values"));
    create_dir(&out_dir_root.join("gradle"));
    create_dir(&out_dir_root.join("gradle/wrapper"));

    // Break app id into list of package folders
    let app_id_dirs: Vec<&str> = DEFAULT_APPLICATION_ID.split('.').collect();

    // Get path to root of java folder
    let mut path_accumulate = out_dir_root.join("app/src/main/java");

    let app_id_dir_iter = {
        if app_id_dirs[0] == "org" {
            path_accumulate.push("org");
            app_id_dirs[1..].iter()
        } else {
            app_id_dirs.iter()
        }
    };

    // Create java folders
    for package in app_id_dir_iter {
        path_accumulate.push(*package);
        create_dir(&path_accumulate);
    }
}

fn register_templates(registry: &mut handlebars::Handlebars) {
    // Gradle
    let gradle = include_str!("../../../templates/android-project/app/build.gradle");
    registry.register_template_string("gradle", gradle).unwrap();

    // Android Manifest
    let manifest =
        include_str!("../../../templates/android-project/app/src/main/AndroidManifest.xml");
    registry
        .register_template_string("manifest", manifest)
        .unwrap();

    // Strings file
    let strings =
        include_str!("../../../templates/android-project/app/src/main/res/values/strings.xml");
    registry
        .register_template_string("strings", strings)
        .unwrap();

    // Activity file
    let activity =
        include_str!("../../../templates/android-project/app/src/main/java/NovaGameActivity.java");
    registry
        .register_template_string("activity", activity)
        .unwrap();
}
