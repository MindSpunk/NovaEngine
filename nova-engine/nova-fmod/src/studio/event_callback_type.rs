/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use raw::FMOD_STUDIO_EVENT_CALLBACK_ALL;
use raw::FMOD_STUDIO_EVENT_CALLBACK_CREATED;
use raw::FMOD_STUDIO_EVENT_CALLBACK_CREATE_PROGRAMMER_SOUND;
use raw::FMOD_STUDIO_EVENT_CALLBACK_DESTROYED;
use raw::FMOD_STUDIO_EVENT_CALLBACK_DESTROY_PROGRAMMER_SOUND;
use raw::FMOD_STUDIO_EVENT_CALLBACK_PLUGIN_CREATED;
use raw::FMOD_STUDIO_EVENT_CALLBACK_PLUGIN_DESTROYED;
use raw::FMOD_STUDIO_EVENT_CALLBACK_REAL_TO_VIRTUAL;
use raw::FMOD_STUDIO_EVENT_CALLBACK_RESTARTED;
use raw::FMOD_STUDIO_EVENT_CALLBACK_SOUND_PLAYED;
use raw::FMOD_STUDIO_EVENT_CALLBACK_SOUND_STOPPED;
use raw::FMOD_STUDIO_EVENT_CALLBACK_STARTED;
use raw::FMOD_STUDIO_EVENT_CALLBACK_STARTING;
use raw::FMOD_STUDIO_EVENT_CALLBACK_START_FAILED;
use raw::FMOD_STUDIO_EVENT_CALLBACK_STOPPED;
use raw::FMOD_STUDIO_EVENT_CALLBACK_TIMELINE_BEAT;
use raw::FMOD_STUDIO_EVENT_CALLBACK_TIMELINE_MARKER;
use raw::FMOD_STUDIO_EVENT_CALLBACK_TYPE;
use raw::FMOD_STUDIO_EVENT_CALLBACK_VIRTUAL_TO_REAL;

bitflags! {
    ///
    /// A wrapper around the FMOD_STUDIO_EVENT_CALLBACK_TYPE bit flags type
    ///
    pub struct EventCallbackType: FMOD_STUDIO_EVENT_CALLBACK_TYPE {
        const CREATED = FMOD_STUDIO_EVENT_CALLBACK_CREATED;
        const DESTROYED = FMOD_STUDIO_EVENT_CALLBACK_DESTROYED;
        const STARTING = FMOD_STUDIO_EVENT_CALLBACK_STARTING;
        const STARTED = FMOD_STUDIO_EVENT_CALLBACK_STARTED;
        const RESTARTED = FMOD_STUDIO_EVENT_CALLBACK_RESTARTED;
        const STOPPED = FMOD_STUDIO_EVENT_CALLBACK_STOPPED;
        const START_FAILED = FMOD_STUDIO_EVENT_CALLBACK_START_FAILED;
        const CREATE_PROGRAMMER_SOUND = FMOD_STUDIO_EVENT_CALLBACK_CREATE_PROGRAMMER_SOUND;
        const DESTROY_PROGRAMMER_SOUND = FMOD_STUDIO_EVENT_CALLBACK_DESTROY_PROGRAMMER_SOUND;
        const PLUGIN_CREATED = FMOD_STUDIO_EVENT_CALLBACK_PLUGIN_CREATED;
        const PLUGIN_DESTROYED = FMOD_STUDIO_EVENT_CALLBACK_PLUGIN_DESTROYED;
        const TIMELINE_MARKER = FMOD_STUDIO_EVENT_CALLBACK_TIMELINE_MARKER;
        const TIMELINE_BEAT = FMOD_STUDIO_EVENT_CALLBACK_TIMELINE_BEAT;
        const SOUND_PLAYED = FMOD_STUDIO_EVENT_CALLBACK_SOUND_PLAYED;
        const SOUND_STOPPED = FMOD_STUDIO_EVENT_CALLBACK_SOUND_STOPPED;
        const REAL_TO_VIRTUAL = FMOD_STUDIO_EVENT_CALLBACK_REAL_TO_VIRTUAL;
        const VIRTUAL_TO_REAL = FMOD_STUDIO_EVENT_CALLBACK_VIRTUAL_TO_REAL;
        const ALL = FMOD_STUDIO_EVENT_CALLBACK_ALL;
    }
}
