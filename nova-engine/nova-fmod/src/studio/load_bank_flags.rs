/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use raw::FMOD_STUDIO_LOAD_BANK_DECOMPRESS_SAMPLES;
use raw::FMOD_STUDIO_LOAD_BANK_FLAGS;
use raw::FMOD_STUDIO_LOAD_BANK_NONBLOCKING;
use raw::FMOD_STUDIO_LOAD_BANK_NORMAL;
use raw::FMOD_STUDIO_LOAD_BANK_UNENCRYPTED;

bitflags! {
    ///
    /// A wrapper around the FMOD_STUDIO_LOAD_BANK_FLAGS bit flags type
    ///
    pub struct LoadBankFlags: FMOD_STUDIO_LOAD_BANK_FLAGS {
        const NORMAL = FMOD_STUDIO_LOAD_BANK_NORMAL;
        const NONBLOCKING = FMOD_STUDIO_LOAD_BANK_NONBLOCKING;
        const DECOMPRESS_SAMPLES = FMOD_STUDIO_LOAD_BANK_DECOMPRESS_SAMPLES;
        const UNENCRYPTED = FMOD_STUDIO_LOAD_BANK_UNENCRYPTED;
    }
}
