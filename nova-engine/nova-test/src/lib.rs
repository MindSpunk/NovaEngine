/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

//#![windows_subsystem = "windows"]
#![allow(clippy::suspicious_else_formatting)]
#![deny(bare_trait_objects)]

// Includes a common set of checks to ensure compiling for the right platform
include!("../../platform_check.rs");

extern crate lyon;
extern crate nova_archive as archive;
extern crate nova_runtime as nova;

use nova::core::application::Application;
use nova::core::input::Event;
use nova::core::nodes::{registed_default_nodes, Camera, DebugCamera, Spatial, StaticMesh};
use nova::core::resources::DeltaTimer;
use nova::core::resources::KeyboardInput;
use nova::core::resources::MouseInput;
use nova::core::resources::WindowResource;
use nova::core::resources::{Controllers, QuitHandle};
use nova::core::systems::Renderer;
use nova::core::systems::{CalculateTransforms, DebugCameraSystem};
use nova::fs::config::MountConfig;
use nova::fs::filesystem::Filesystem;
use nova::math::types::Quat;
use nova::math::types::Vec3;
use nova::universe::{Dependencies, Dependency, TaskBox, TaskGraph, Universe};

pub fn nova_main() {
    Application::run(game_main);
}

pub fn game_main(app: Application) {
    // =============================================================================================
    // Allocate approximately 32MB for the virtual filesystem
    // =============================================================================================
    let mut fs_memory = vec![0u8; 16777215 * 2];

    // =============================================================================================
    // Load the virtual filesystem root config
    // =============================================================================================
    let config = MountConfig::load("Mount.toml").unwrap();

    // =============================================================================================
    // Build the allocators for the virtual filesystem
    // =============================================================================================
    let (files, packages) = Filesystem::storage(&mut fs_memory);

    // =============================================================================================
    // Build the virtual filesystem
    // =============================================================================================
    let fs = Filesystem::builder()
        .config(config)
        .build(&files, &packages)
        .unwrap();
    let fs = nova::core::resources::Filesystem::new(fs);

    // =============================================================================================
    //  Setup the Input and Timer
    // =============================================================================================
    let controllers = Controllers::new(app.controller_buffer);
    let keyboard_input = KeyboardInput::new();
    let mouse_input = MouseInput::new(app.mouse);
    let delta_timer = DeltaTimer::new(app.timer);

    // =============================================================================================
    //  Create a Renderer
    // =============================================================================================
    let renderer = Renderer::new(&app.window, &fs);

    // =============================================================================================
    //  Setup the Universe
    // =============================================================================================
    let mut universe = Universe::new();
    registed_default_nodes(&mut universe);

    // =============================================================================================
    //  Register the resources with the Universe
    // =============================================================================================
    let window = WindowResource::new(app.window);
    let player_controls = DebugCameraSystem::new();
    let quit_handle = QuitHandle::new();
    universe.add_resource(fs);
    universe.add_resource(quit_handle.clone());
    universe.add_resource(window);
    universe.add_resource(controllers);
    universe.add_resource(keyboard_input);
    universe.add_resource(mouse_input);
    universe.add_resource(delta_timer);
    universe.add_resource(player_controls);
    universe.add_resource(renderer);

    // =============================================================================================
    //  Add the player's controlled camera
    // =============================================================================================
    let dcam_node = universe.node_builder(DebugCamera::new()).build().unwrap();

    universe
        .node_builder(Camera::default())
        .parent(dcam_node)
        .build()
        .unwrap();

    // =============================================================================================
    //  Add an axis group
    // =============================================================================================
    let axis = universe.node_builder(Spatial::new()).build().unwrap();

    universe
        .node_builder(StaticMesh::new())
        .position(Vec3::right() * 0.5)
        .scale(Vec3::new(1.0, 0.1, 0.1))
        .parent(axis)
        .build()
        .unwrap();

    universe
        .node_builder(StaticMesh::new())
        .position(Vec3::up() * 0.5)
        .scale(Vec3::new(0.1, 1.0, 0.1))
        .parent(axis)
        .build()
        .unwrap();

    universe
        .node_builder(StaticMesh::new())
        .position(Vec3::forward() * 0.5)
        .scale(Vec3::new(0.1, 0.1, 1.0))
        .parent(axis)
        .build()
        .unwrap();

    // =============================================================================================
    //  Add the mesh we want to render
    // =============================================================================================
    let mut spawn_pillar = |x: f32, angle: f32| {
        universe
            .node_builder(StaticMesh::new())
            .position(Vec3::new(x, 0.0, 10.0))
            .rotation(Quat::from_angle_axis(angle, Vec3::up()))
            .scale(Vec3::one())
            .build()
            .unwrap();

        universe
            .node_builder(StaticMesh::new())
            .position(Vec3::new(x, 5.0, 10.0))
            .rotation(Quat::from_angle_axis(angle, Vec3::up()))
            .scale(Vec3::new(2.0, 2.0, 2.0))
            .build()
            .unwrap();

        universe
            .node_builder(StaticMesh::new())
            .position(Vec3::new(x, -5.0, 10.0))
            .rotation(Quat::from_angle_axis(angle, Vec3::up()))
            .scale(Vec3::new(5.0, 5.0, 5.0))
            .build()
            .unwrap();
    };
    spawn_pillar(0f32, 0f32);
    spawn_pillar(5f32, 0f32);
    spawn_pillar(-5f32, 0f32);

    universe
        .node_builder(StaticMesh::new())
        .position([0.0, 0.0, 8.0].into())
        .scale(Vec3::new(0.1, 0.1, 0.1))
        .build()
        .unwrap();

    // =============================================================================================
    //  Setup our task graph
    // =============================================================================================
    let mut task_graph = TaskGraph::builder();

    DebugCameraSystem::schedule_task(&mut task_graph);
    CalculateTransforms::schedule_task(&mut task_graph);
    Renderer::schedule_task(&mut task_graph);

    let task_graph = task_graph.build();

    // =============================================================================================
    //  Run our own task for handling input and the event loop
    // =============================================================================================
    let input_task = TaskBox::new(|access| {
        let controllers = access.get_resource_mut::<Controllers>().unwrap();
        let keyboard_input = access.get_resource_mut::<KeyboardInput>().unwrap();
        let mouse_input = access.get_resource_mut::<MouseInput>().unwrap();
        let delta_timer = access.get_resource_mut::<DeltaTimer>().unwrap();
        let window = access.get_resource_ref::<WindowResource>().unwrap();
        let quit_handle = access.get_resource_ref::<QuitHandle>().unwrap();

        // Tick the delta timer
        delta_timer.frame();

        // =========================================================================================
        //  Get a list of events produced since last drain. Having a Vec of them makes things easier
        // =========================================================================================
        let event_list = window.drain_events();

        // =========================================================================================
        //  Start handling events
        // =========================================================================================
        // the Controllers resource manages the controller events to manage a list of connected
        // controllers
        controllers.frame();
        keyboard_input.handle_events(&event_list);
        mouse_input.handle_events(&event_list);

        for event in event_list.iter() {
            match event {
                Event::Quit { .. } => {
                    quit_handle.quit();
                }
                _ => {}
            }
        }
    });
    let input_task_deps = |deps: &mut Dependencies| {
        deps.write(Dependency::resource::<Controllers>());
        deps.write(Dependency::resource::<KeyboardInput>());
        deps.write(Dependency::resource::<MouseInput>());
        deps.write(Dependency::resource::<DeltaTimer>());
        deps.read(Dependency::resource::<WindowResource>());
        deps.read(Dependency::resource::<QuitHandle>());
    };

    // =============================================================================================
    //  Begin main loop
    // =============================================================================================
    while !quit_handle.should_quit() {
        // =========================================================================================
        //  Dispatch a run of the task graph
        // =========================================================================================
        universe.execute_task(&input_task, &input_task_deps);
        task_graph.execute(&mut universe);
    }
}
