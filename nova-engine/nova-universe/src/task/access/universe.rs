/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::arena::NodeArena;
use crate::universe::{
    node_storage_get_node_mut, node_storage_get_node_ref, node_storage_is_valid,
    universe_create_child_node, universe_create_node, universe_erase_node, universe_remove_node,
    universe_remove_node_reparent, NodeHierarchyStorage,
};
use crate::{
    MatrixGroupMut, Node, NodeHierarchyMut, NodeReference, NodesMut, TNodeReference, Transform3D,
    TransformGroupMut,
};
use nova_math::types::Mat4x4;
use std::any::TypeId;
use std::cell::UnsafeCell;
use std::collections::HashMap;

///
/// A builder for TaskUniverse
///
pub struct TaskUniverseBuilder<'a, 'b, 'c, 'd> {
    inner: TaskUniverse<'a, 'b, 'c, 'd>,
}

impl<'a, 'b, 'c, 'd> TaskUniverseBuilder<'a, 'b, 'c, 'd> {
    pub fn new(
        transforms: TransformGroupMut<'b>,
        matrices: MatrixGroupMut<'c>,
        hierarchy: NodeHierarchyMut<'d>,
    ) -> Self {
        let transforms = unsafe { &mut *transforms.transforms };
        let matrices = unsafe { &mut *matrices.matrices };
        let hierarchy = unsafe { &mut *hierarchy.node_hierarchy };
        let inner = TaskUniverse {
            nodes: Default::default(),
            transforms,
            matrices,
            hierarchy,
        };
        Self { inner }
    }

    pub fn add_node_type<T: Node>(mut self, nodes: NodesMut<'a, T>) -> Self {
        let nodes_ref = nodes.arena;
        self.inner
            .nodes
            .insert(TypeId::of::<T>(), UnsafeCell::new(nodes_ref));
        self
    }

    pub fn build(self) -> TaskUniverse<'a, 'b, 'c, 'd> {
        self.inner
    }
}

///
/// Mutable access to the Universe that can add/remove nodes inside a task
///
pub struct TaskUniverse<'a, 'b, 'c, 'd> {
    pub(crate) nodes: HashMap<TypeId, UnsafeCell<&'a mut NodeArena>>,
    pub(crate) transforms: &'b mut HashMap<TypeId, UnsafeCell<Vec<Transform3D>>>,
    pub(crate) matrices: &'c mut HashMap<TypeId, UnsafeCell<Vec<Mat4x4>>>,
    pub(crate) hierarchy: &'d mut NodeHierarchyStorage,
}

impl<'a, 'b, 'c, 'd> TaskUniverse<'a, 'b, 'c, 'd> {
    ///
    /// Creates a builder
    ///
    pub fn builder(
        transforms: TransformGroupMut<'b>,
        matrices: MatrixGroupMut<'c>,
        hierarchy: NodeHierarchyMut<'d>,
    ) -> TaskUniverseBuilder<'a, 'b, 'c, 'd> {
        TaskUniverseBuilder::new(transforms, matrices, hierarchy)
    }

    ///
    ///
    ///
    pub fn get_node_ref<T: Node>(&self, node: TNodeReference<T>) -> Option<&T> {
        let node_arena = unsafe { &*self.nodes.get(&TypeId::of::<T>()).unwrap().get() };
        node_storage_get_node_ref(node_arena, node)
    }

    ///
    ///
    ///
    pub fn get_node_mut<T: Node>(&mut self, node: TNodeReference<T>) -> Option<&mut T> {
        let node_arena = unsafe { &mut *self.nodes.get_mut(&TypeId::of::<T>()).unwrap().get() };
        node_storage_get_node_mut(node_arena, node)
    }

    ///
    ///
    ///
    pub fn get_node_transform_ref<R: Into<NodeReference>>(&self, node: R) -> Option<&Transform3D> {
        let node: NodeReference = node.into();
        if self.is_valid(node) {
            let transforms = self.transforms.get(&node.node_type()).unwrap();
            let transforms = unsafe { &*transforms.get() };
            let transform = &transforms[node.index.index as usize];
            Some(transform)
        } else {
            None
        }
    }

    ///
    ///
    ///
    pub fn get_node_transform_mut<R: Into<NodeReference>>(
        &mut self,
        node: R,
    ) -> Option<&mut Transform3D> {
        let node: NodeReference = node.into();
        if self.is_valid(node) {
            let transforms = self.transforms.get_mut(&node.node_type()).unwrap();
            let transforms = unsafe { &mut *transforms.get() };
            let transform = &mut transforms[node.index.index as usize];
            Some(transform)
        } else {
            None
        }
    }

    ///
    ///
    ///
    pub fn get_node_matrix_ref<R: Into<NodeReference>>(&self, node: R) -> Option<&Mat4x4> {
        let node: NodeReference = node.into();
        if self.is_valid(node) {
            let matrices = self.matrices.get(&node.node_type()).unwrap();
            let matrices = unsafe { &*matrices.get() };
            let matrix = &matrices[node.index.index as usize];
            Some(matrix)
        } else {
            None
        }
    }

    ///
    ///
    ///
    pub fn get_node_matrix_mut<R: Into<NodeReference>>(&mut self, node: R) -> Option<&mut Mat4x4> {
        let node: NodeReference = node.into();
        if self.is_valid(node) {
            let matrices = self.matrices.get_mut(&node.node_type()).unwrap();
            let matrices = unsafe { &mut *matrices.get() };
            let matrix = &mut matrices[node.index.index as usize];
            Some(matrix)
        } else {
            None
        }
    }

    ///
    ///
    ///
    pub fn create_node<T: Node>(&mut self, v: T, transform: Transform3D) -> TNodeReference<T> {
        let node_arena = unsafe {
            let nodes = self.nodes.get_mut(&TypeId::of::<T>()).unwrap();
            let nodes = &mut *nodes.get();
            &mut *nodes
        };
        let node_transforms = unsafe {
            let nodes = self.transforms.get_mut(&TypeId::of::<T>()).unwrap();
            &mut *nodes.get()
        };
        let node_matrices = unsafe {
            let nodes = self.matrices.get_mut(&TypeId::of::<T>()).unwrap();
            &mut *nodes.get()
        };
        let node_hierarchy = &mut self.hierarchy;
        universe_create_node(
            node_arena,
            node_transforms,
            node_matrices,
            node_hierarchy,
            v,
            transform,
        )
    }

    ///
    ///
    ///
    pub fn create_child_node<T: Node, R: Into<NodeReference>>(
        &mut self,
        v: T,
        transform: Transform3D,
        parent: R,
    ) -> Option<TNodeReference<T>> {
        let node_arenas = &mut self.nodes;
        let node_transforms = unsafe {
            let nodes = self.transforms.get_mut(&TypeId::of::<T>()).unwrap();
            &mut *nodes.get()
        };
        let node_matrices = unsafe {
            let nodes = self.matrices.get_mut(&TypeId::of::<T>()).unwrap();
            &mut *nodes.get()
        };
        let node_hierarchy = &mut self.hierarchy;

        universe_create_child_node(
            node_arenas,
            node_transforms,
            node_matrices,
            node_hierarchy,
            v,
            transform,
            parent,
        )
    }

    //pub fn node_builder<T: Node>(&mut self, node: T) -> NodeBuilder<'_, 'resources, T> {
    //    NodeBuilder {
    //        universe: self,
    //        node,
    //        transform: Default::default(),
    //        parent: NodeReference::null(),
    //    }
    //}

    ///
    ///
    ///
    pub fn remove_node<T: Node>(&mut self, node: TNodeReference<T>) -> T {
        let node_arenas = &mut self.nodes;
        let node_hierarchy = &mut self.hierarchy;
        universe_remove_node(node_arenas, node_hierarchy, node)
    }

    ///
    ///
    ///
    pub fn erase_node<R: Into<NodeReference>>(&mut self, node: R) {
        let node_arenas = &mut self.nodes;
        let node_hierarchy = &mut self.hierarchy;
        universe_erase_node(node_arenas, node_hierarchy, node)
    }

    ///
    ///
    ///
    pub fn remove_node_reparent<R: Into<NodeReference>>(&mut self, node: R, new_parent: R) {
        let node = node.into();
        let new_parent = new_parent.into();

        let node_arenas = &mut self.nodes;
        let node_hierarchy = &mut self.hierarchy;

        universe_remove_node_reparent(node_arenas, node_hierarchy, node, new_parent)
    }

    ///
    ///
    ///
    pub fn is_valid<R: Into<NodeReference>>(&self, node: R) -> bool {
        let node = node.into();
        let node_type = node.node_type();
        let node_arena = unsafe {
            let node_arena = self.nodes.get(&node_type).unwrap().get();
            &mut *node_arena
        };
        node_storage_is_valid(*node_arena, node)
    }
}
