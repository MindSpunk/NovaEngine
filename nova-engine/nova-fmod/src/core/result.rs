/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use raw::FMOD_RESULT;

///
/// A rusty wrapper for FMOD_RESULT that maps ERR_OK to Ok and the rest of the values in the enum to
/// Err.
///
pub type Result<T> = std::result::Result<T, Error>;

///
/// Internal utility function used for mapping an FMOD_RESULT to a Result
///
pub(crate) unsafe fn make_result<T>(val: T, err: FMOD_RESULT) -> Result<T> {
    if err == FMOD_RESULT::FMOD_OK {
        Ok(val)
    } else {
        Err(std::mem::transmute(err))
    }
}

///
/// A rustier version of the FMOD_RESULT enum with the ERR_OK value removed. This makes this enum
/// function like an error code rather. This type should be used wrapped in a Result
///
#[repr(u32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Error {
    BadCommand = 1,
    ChannelAlloc = 2,
    ChannelStolen = 3,
    Dma = 4,
    DspConnection = 5,
    DspDontProcess = 6,
    DspFormat = 7,
    DspInuse = 8,
    DspNotfound = 9,
    DspReserved = 10,
    DspSilence = 11,
    DspType = 12,
    FileBad = 13,
    FileCouldNotSeek = 14,
    FileDiskEjected = 15,
    FileEof = 16,
    FileEndOfData = 17,
    FileNotFound = 18,
    Format = 19,
    HeaderMismatch = 20,
    Http = 21,
    HttpAccess = 22,
    HttpProxyAuth = 23,
    HttpServerError = 24,
    HttpTimeout = 25,
    Initialization = 26,
    Initialized = 27,
    Internal = 28,
    InvalidFloat = 29,
    InvalidHandle = 30,
    InvalidParam = 31,
    InvalidPosition = 32,
    InvalidSpeaker = 33,
    InvalidSyncPoint = 34,
    InvalidThread = 35,
    InvalidVector = 36,
    MaxAudible = 37,
    Memory = 38,
    MemoryCantPoint = 39,
    Needs3d = 40,
    NeedsHardware = 41,
    NetConnect = 42,
    NetSocketError = 43,
    NetUrl = 44,
    NetWouldBlock = 45,
    NotReady = 46,
    OutputAllocated = 47,
    OutputCreateBuffer = 48,
    OutputDriverCall = 49,
    OutputFormat = 50,
    OutputInit = 51,
    OutputNoDrivers = 52,
    Plugin = 53,
    PluginMissing = 54,
    PluginResource = 55,
    PluginVersion = 56,
    Record = 57,
    ReverbChannelGroup = 58,
    ReverbInstance = 59,
    SubSounds = 60,
    SubSoundAllocated = 61,
    SubSoundCantMove = 62,
    TagNotFound = 63,
    TooManyChannels = 64,
    Truncated = 65,
    Unimplemented = 66,
    Uninitialized = 67,
    Unsupported = 68,
    Version = 69,
    EventAlreadyLoaded = 70,
    EventLiveUpdateBusy = 71,
    EventLiveUpdateMismatch = 72,
    EventLiveUpdateTimeout = 73,
    EventNotfound = 74,
    StudioUninitialized = 75,
    StudioNotLoaded = 76,
    InvalidString = 77,
    AlreadyLocked = 78,
    NotLocked = 79,
    RecordDisconnected = 80,
    TooManySamples = 81,
}

impl Error {
    pub fn error_string(&self) -> &'static str {
        match self
        {
            Error::BadCommand => "Tried to call a function on a data type that does not allow this type of functionality (ie calling Sound::lock on a streaming sound).",
            Error::ChannelAlloc => "Error trying to allocate a channel.",
            Error::ChannelStolen => "The specified channel has been reused to play another sound.",
            Error::Dma => "DMA Failure.  See debug output for more information.",
            Error::DspConnection => "DSP connection error.  Connection possibly caused a cyclic dependency or connected dsps with incompatible buffer counts.",
            Error::DspDontProcess => "DSP return code from a DSP process query callback.  Tells mixer not to call the process callback and therefore not consume CPU.  Use this to optimize the DSP graph.",
            Error::DspFormat => "DSP Format error.  A DSP unit may have attempted to connect to this network with the wrong format, or a matrix may have been set with the wrong size if the target unit has a specified channel map.",
            Error::DspInuse => "DSP is already in the mixer's DSP network. It must be removed before being reinserted or released.",
            Error::DspNotfound => "DSP connection error.  Couldn't find the DSP unit specified.",
            Error::DspReserved => "DSP operation error.  Cannot perform operation on this DSP as it is reserved by the system.",
            Error::DspSilence => "DSP return code from a DSP process query callback.  Tells mixer silence would be produced from read, so go idle and not consume CPU.  Use this to optimize the DSP graph.",
            Error::DspType => "DSP operation cannot be performed on a DSP of this type.",
            Error::FileBad => "Error loading file.",
            Error::FileCouldNotSeek => "Couldn't perform seek operation.  This is a limitation of the medium (ie netstreams) or the file format.",
            Error::FileDiskEjected => "Media was ejected while reading.",
            Error::FileEof => "End of file unexpectedly reached while trying to read essential data (truncated?).",
            Error::FileEndOfData => "End of current chunk reached while trying to read data.",
            Error::FileNotFound => "File not found.",
            Error::Format => "Unsupported file or audio format.",
            Error::HeaderMismatch => "There is a version mismatch between the FMOD header and either the FMOD Studio library or the FMOD Low Level library.",
            Error::Http => "A HTTP error occurred. This is a catch-all for HTTP errors not listed elsewhere.",
            Error::HttpAccess => "The specified resource requires authentication or is forbidden.",
            Error::HttpProxyAuth => "Proxy authentication is required to access the specified resource.",
            Error::HttpServerError => "A HTTP server error occurred.",
            Error::HttpTimeout => "The HTTP request timed out.",
            Error::Initialization => "FMOD was not initialized correctly to support this function.",
            Error::Initialized => "Cannot call this command after System::init.",
            Error::Internal => "An error occurred that wasn't supposed to.  Contact support.",
            Error::InvalidFloat => "Value passed in was a NaN, Inf or denormalized float.",
            Error::InvalidHandle => "An invalid object handle was used.",
            Error::InvalidParam => "An invalid parameter was passed to this function.",
            Error::InvalidPosition => "An invalid seek position was passed to this function.",
            Error::InvalidSpeaker => "An invalid speaker was passed to this function based on the current speaker mode.",
            Error::InvalidSyncPoint => "The syncpoint did not come from this sound handle.",
            Error::InvalidThread => "Tried to call a function on a thread that is not supported.",
            Error::InvalidVector => "The vectors passed in are not unit length, or perpendicular.",
            Error::MaxAudible => "Reached maximum audible playback count for this sound's soundgroup.",
            Error::Memory => "Not enough memory or resources.",
            Error::MemoryCantPoint => "Can't use FMOD_OPENMEMORY_POINT on non PCM source data, or non mp3/xma/adpcm data if FMOD_CREATECOMPRESSEDSAMPLE was used.",
            Error::Needs3d => "Tried to call a command on a 2d sound when the command was meant for 3d sound.",
            Error::NeedsHardware => "Tried to use a feature that requires hardware support.",
            Error::NetConnect => "Couldn't connect to the specified host.",
            Error::NetSocketError => "A socket error occurred.  This is a catch-all for socket-related errors not listed elsewhere.",
            Error::NetUrl => "The specified URL couldn't be resolved.",
            Error::NetWouldBlock => "Operation on a non-blocking socket could not complete immediately.",
            Error::NotReady => "Operation could not be performed because specified sound/DSP connection is not ready.",
            Error::OutputAllocated => "Error initializing output device, but more specifically, the output device is already in use and cannot be reused.",
            Error::OutputCreateBuffer => "Error creating hardware sound buffer.",
            Error::OutputDriverCall => "A call to a standard soundcard driver failed, which could possibly mean a bug in the driver or resources were missing or exhausted.",
            Error::OutputFormat => "Soundcard does not support the specified format.",
            Error::OutputInit => "Error initializing output device.",
            Error::OutputNoDrivers => "The output device has no drivers installed.  If pre-init, FMOD_OUTPUT_NOSOUND is selected as the output mode.  If post-init, the function just fails.",
            Error::Plugin => "An unspecified error has been returned from a plugin.",
            Error::PluginMissing => "A requested output, dsp unit type or codec was not available.",
            Error::PluginResource => "A resource that the plugin requires cannot be found. (ie the DLS file for MIDI playback)",
            Error::PluginVersion => "A plugin was built with an unsupported SDK version.",
            Error::Record => "An error occurred trying to initialize the recording device.",
            Error::ReverbChannelGroup => "Reverb properties cannot be set on this channel because a parent channelgroup owns the reverb connection.",
            Error::ReverbInstance => "Specified instance in FMOD_REVERB_PROPERTIES couldn't be set. Most likely because it is an invalid instance number or the reverb doesn't exist.",
            Error::SubSounds => "The error occurred because the sound referenced contains subsounds when it shouldn't have, or it doesn't contain subsounds when it should have.  The operation may also not be able to be performed on a parent sound.",
            Error::SubSoundAllocated => "This subsound is already being used by another sound, you cannot have more than one parent to a sound.  Null out the other parent's entry first.",
            Error::SubSoundCantMove => "Shared subsounds cannot be replaced or moved from their parent stream, such as when the parent stream is an FSB file.",
            Error::TagNotFound => "The specified tag could not be found or there are no tags.",
            Error::TooManyChannels => "The sound created exceeds the allowable input channel count.  This can be increased using the 'maxinputchannels' parameter in System::setSoftwareFormat.",
            Error::Truncated => "The retrieved string is too long to fit in the supplied buffer and has been truncated.",
            Error::Unimplemented => "Something in FMOD hasn't been implemented when it should be! contact support!",
            Error::Uninitialized => "This command failed because System::init or System::setDriver was not called.",
            Error::Unsupported => "A command issued was not supported by this object.  Possibly a plugin without certain callbacks specified.",
            Error::Version => "The version number of this file format is not supported.",
            Error::EventAlreadyLoaded => "The specified bank has already been loaded.",
            Error::EventLiveUpdateBusy => "The live update connection failed due to the game already being connected.",
            Error::EventLiveUpdateMismatch => "The live update connection failed due to the game data being out of sync with the tool.",
            Error::EventLiveUpdateTimeout => "The live update connection timed out.",
            Error::EventNotfound => "The requested event, parameter, bus or vca could not be found.",
            Error::StudioUninitialized => "The Studio::System object is not yet initialized.",
            Error::StudioNotLoaded => "The specified resource is not loaded, so it can't be unloaded.",
            Error::InvalidString => "An invalid string was passed to this function.",
            Error::AlreadyLocked => "The specified resource is already locked.",
            Error::NotLocked => "The specified resource is not locked, so it can't be unlocked.",
            Error::RecordDisconnected => "The specified recording driver has been disconnected.",
            Error::TooManySamples => "The length provided exceeds the allowable limit.",
        }
    }
}
