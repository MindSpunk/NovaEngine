/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

extern crate crossbeam;

use crossbeam::sync::WaitGroup;
use std::ops::Deref;

///
/// An Arc like struct that enforces that the data is destructed on the thread it was created from.
///
/// # Warning
///
/// OwnedArc::drop() will wait for all Ref instances to be dropped before actually completing the
/// drop. This means that the OwnedArc's thread will stall if there are any Ref instances keeping
/// the data alive so take care to make this not be the case.
///
pub struct OwnedArc<T: Sync> {
    val: Box<T>,
    count: WaitGroup,
}

impl<T: Sync> OwnedArc<T> {
    ///
    /// Constructs a new OwnedArc holding the given data
    ///
    pub fn new(val: T) -> Self {
        Self {
            val: Box::new(val),
            count: WaitGroup::new(),
        }
    }

    ///
    /// Gets a weak reference to the underlying data.
    ///
    pub fn weak(&self) -> Weak<T> {
        Weak::<T> {
            ptr: self.val.as_ref() as *const T,
            count: self.count.clone(),
        }
    }
}

unsafe impl<T: Sync> Sync for OwnedArc<T> {}

impl<T: Sync> Deref for OwnedArc<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.val
    }
}

impl<T: Sync> Drop for OwnedArc<T> {
    fn drop(&mut self) {
        let mut actual_wait = WaitGroup::new();
        core::mem::swap(&mut actual_wait, &mut self.count);
        actual_wait.wait();
    }
}

///
/// Holds a reference to data owned by an OwnedArc, without the ability to take ownership. T::drop()
/// will always be called on the thread the OwnedArc is owned by.
///
pub struct Weak<T: Sync> {
    ptr: *const T,
    count: WaitGroup,
}

impl<T: Sync> Clone for Weak<T> {
    fn clone(&self) -> Self {
        Self {
            ptr: self.ptr,
            count: self.count.clone(),
        }
    }
}

unsafe impl<T: Sync> Sync for Weak<T> {}
unsafe impl<T: Sync> Send for Weak<T> {}

impl<T: Sync> Deref for Weak<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        unsafe { &*self.ptr }
    }
}
