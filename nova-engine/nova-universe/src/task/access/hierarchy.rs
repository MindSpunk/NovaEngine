/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::node::NodeItem;
use crate::universe::NodeHierarchyStorage;
use crate::NodeReference;
use std::marker::PhantomData;

// =================================================================================================
///
///
///
#[repr(transparent)]
pub struct NodeHierarchy<'a> {
    pub(crate) node_hierarchy: *const NodeHierarchyStorage,
    pub(crate) phantom: PhantomData<&'a ()>,
}

impl<'a> NodeHierarchy<'a> {
    ///
    ///
    ///
    pub fn get_parent(&self, node: NodeReference) -> NodeReference {
        let item = unsafe { self.get_inner().get(&node).unwrap() };
        item.parent
    }

    ///
    ///
    ///
    pub fn get_children(&self, node: NodeReference) -> &[NodeReference] {
        let item = unsafe { self.get_inner().get(&node).unwrap() };
        &item.children
    }

    pub fn iter_everything(&self) -> impl Iterator<Item = (&NodeReference, &NodeItem)> {
        let inner = unsafe { self.get_inner() };
        inner.iter()
    }

    ///
    ///
    ///
    pub(crate) unsafe fn get_inner(&self) -> &NodeHierarchyStorage {
        &*self.node_hierarchy
    }
}

// =================================================================================================
///
///
///
#[repr(transparent)]
pub struct NodeHierarchyMut<'a> {
    pub(crate) node_hierarchy: *mut NodeHierarchyStorage,
    pub(crate) phantom: PhantomData<&'a mut ()>,
}

impl<'a> NodeHierarchyMut<'a> {
    ///
    ///
    ///
    pub fn get_parent(&self, node: NodeReference) -> NodeReference {
        let item = self.get_inner().get(&node).unwrap();
        item.parent
    }

    ///
    ///
    ///
    pub fn get_children(&self, node: NodeReference) -> &[NodeReference] {
        let item = self.get_inner().get(&node).unwrap();
        &item.children
    }

    ///
    ///
    ///
    pub fn make_parent_node(&mut self, child: NodeReference, parent: NodeReference) {
        let item = self.get_inner_mut().get_mut(&child).unwrap();

        let old_parent = item.parent;
        item.parent = parent;

        if !old_parent.is_null() {
            let item = self.get_inner_mut().get_mut(&old_parent).unwrap();
            let i = item.children.iter().position(|x| x == &child).unwrap();
            item.children.remove(i);
        }

        let item = self.get_inner_mut().get_mut(&parent).unwrap();

        if !item.children.contains(&child) {
            item.children.push(child);
        }
    }

    ///
    ///
    ///
    pub fn unparent_node(&mut self, node: NodeReference) {
        let item = self.get_inner_mut().get_mut(&node).unwrap();

        let old_parent = item.parent;
        item.parent = NodeReference::null();

        if !old_parent.is_null() {
            let item = self.get_inner_mut().get_mut(&old_parent).unwrap();
            let i = item.children.iter().position(|x| x == &node).unwrap();
            item.children.remove(i);
        }
    }

    ///
    ///
    ///
    pub(crate) fn get_inner(&self) -> &NodeHierarchyStorage {
        unsafe { &*self.node_hierarchy }
    }

    ///
    ///
    ///
    pub(crate) fn get_inner_mut(&mut self) -> &mut NodeHierarchyStorage {
        unsafe { &mut *self.node_hierarchy }
    }
}
