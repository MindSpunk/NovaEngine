/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::math::types::std140;
use crate::vulkan::VertexFormat;

use ash::vk;

#[repr(C)]
#[derive(Clone, Debug)]
pub struct VertexData {
    pub position: std140::Vec3P,
    pub normal: std140::Vec3P,
    pub tangent: std140::Vec3P,
    pub uv: std140::Vec3P,
}

impl VertexFormat for VertexData {
    fn input_description() -> vk::PipelineVertexInputStateCreateInfo {
        static BINDING_DESC: vk::VertexInputBindingDescription =
            vk::VertexInputBindingDescription {
                binding: 0,
                stride: 11 * 4,
                input_rate: vk::VertexInputRate::VERTEX,
            };

        static POS_DESC: vk::VertexInputAttributeDescription =
            vk::VertexInputAttributeDescription {
                location: 0,
                binding: 0,
                format: vk::Format::R32G32B32_SFLOAT,
                offset: 0,
            };

        static NRM_DESC: vk::VertexInputAttributeDescription =
            vk::VertexInputAttributeDescription {
                location: 1,
                binding: 0,
                format: vk::Format::R32G32B32_SFLOAT,
                offset: 3 * 4,
            };

        static TAN_DESC: vk::VertexInputAttributeDescription =
            vk::VertexInputAttributeDescription {
                location: 2,
                binding: 0,
                format: vk::Format::R32G32B32_SFLOAT,
                offset: 6 * 4,
            };

        static UV_DESC: vk::VertexInputAttributeDescription = vk::VertexInputAttributeDescription {
            location: 3,
            binding: 0,
            format: vk::Format::R32G32_SFLOAT,
            offset: 9 * 4,
        };

        static BINDINGS: [vk::VertexInputBindingDescription; 1] = [BINDING_DESC];
        static ATTRS: [vk::VertexInputAttributeDescription; 4] =
            [POS_DESC, NRM_DESC, TAN_DESC, UV_DESC];

        vk::PipelineVertexInputStateCreateInfo::builder()
            .vertex_binding_descriptions(&BINDINGS)
            .vertex_attribute_descriptions(&ATTRS)
            .build()
    }
}
