/*
 *
 * This file is a part of NovaEngine
 * https://gitlab.com/MindSpunk/NovaEngine
 *
 *
 * MIT License
 *
 * Copyright (c) 2020 Nathan Voglsam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use crate::raw::{
    FMOD_FILE_CLOSE_CALLBACK, FMOD_FILE_OPEN_CALLBACK, FMOD_FILE_READ_CALLBACK,
    FMOD_FILE_SEEK_CALLBACK,
};
use std::os::raw::{c_int, c_void};

///
/// FMOD_STUDIO_BANK_INFO
///
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct BankInfo {
    pub size: c_int,
    pub userdata: *mut c_void,
    pub userdatalength: c_int,
    pub opencallback: FMOD_FILE_OPEN_CALLBACK,
    pub closecallback: FMOD_FILE_CLOSE_CALLBACK,
    pub readcallback: FMOD_FILE_READ_CALLBACK,
    pub seekcallback: FMOD_FILE_SEEK_CALLBACK,
}

impl BankInfo {
    pub fn from_ffi(ffi: raw::FMOD_STUDIO_BANK_INFO) -> Self {
        Self {
            size: ffi.size,
            userdata: ffi.userdata,
            userdatalength: ffi.userdatalength,
            opencallback: ffi.opencallback,
            closecallback: ffi.closecallback,
            readcallback: ffi.readcallback,
            seekcallback: ffi.seekcallback,
        }
    }

    pub fn into_ffi(self) -> raw::FMOD_STUDIO_BANK_INFO {
        raw::FMOD_STUDIO_BANK_INFO {
            size: self.size,
            userdata: self.userdata,
            userdatalength: self.userdatalength,
            opencallback: self.opencallback,
            closecallback: self.closecallback,
            readcallback: self.readcallback,
            seekcallback: self.seekcallback,
        }
    }
}
